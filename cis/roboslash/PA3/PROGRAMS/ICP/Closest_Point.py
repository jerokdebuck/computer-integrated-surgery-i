#module Closest_point

import numpy as np

'''
    A closest point algorithm for finding the closest point
    to a surface abstracted by 3 points in 3d space. Implemented
    using baryocentric interpolation
'''

class Closest_Point(object):

    # initialize and find closest point
    def __init__(self, a, points):
        p, q, r = points[0], points[1], points[2]

        ap = np.transpose(a) - p
        ap = np.transpose(ap)

        A = np.transpose(np.matrix(q-p))
        A = np.concatenate( (A, np.transpose(np.asmatrix(r - p))), axis=1)

        coeffs = np.linalg.lstsq(A, ap)
        # Acquire coefficients lambda and mu
        lam, mu = coeffs[0][0], coeffs[0][1]

        self.c = p + lam*(q-p) + mu*(r-p)
        tot = lam + mu

        ''' The following is logic for determining if
            the current closest point is a baryocentric point
            of the triangle. It could be the case where
            one or two conditions fail, in which case the
            closest point is projected on a segment (one fails)
            or is simply a vertex of the triangle (two fails)
        '''

        # lambda has failed test
        if lam < 0 and mu >= 0 and tot <= 1:
            self.c = self.ProjectOnSegment(self.c, r, p)
        # both lambda nad mu have failed
        elif lam < 0 and mu < 0 and tot <= 1:
            self.c = p
        # both mu and mu + lambda have failed
        elif lam < 0 and mu >= 0 and tot > 1:
            self.c = r
        # mu has failed
        elif lam >= 0 and mu < 0 and tot <= 1:
            self.c = self.ProjectOnSegment(self.c, p, q)
        # lambda and lambda + mu have failed
        elif lam >= 0 and mu < 0 and tot > 1:
            self.c = q
        # mu + lambda has failed
        elif lam >= 0 and mu >= 0 and tot > 1:
            self.c = self.ProjectOnSegment(self.c, q, r)
        # if none fail, keep c as is
        self.c = np.asmatrix(self.c)

    # finds a new c_opt which is projected on segment p, q
    def ProjectOnSegment(self, c, p, q):
        lam = (np.dot( (c-p), np.transpose((q-p)) )) / (np.dot( (q-p), np.transpose(q-p)) )
        lam_opt = max(0, min(lam, 1))
        c_opt = p + lam_opt * (q-p)
        return c_opt

    # return the closet point
    def return_closest_point(self):
        return self.c
