
#module AdvancedICP
import OctTree as OctTree
from Transformation import Frame_Transformation
from Containers import PointCloud
import numpy as np
import math
import sys
'''
AdvancedICP represnts a more advanced way of performing iterative closest
point algorithm. An oct tree is built to split the entire point space
into hierarchiac fragments, which each node contains a subet of the
space based on certain bounds. AdvancedICP builds this tree and simply
finds the closest point for all s_k of interest
'''

class AdvancedICP(object):

    def __init__(self, vertices, indices, d_list):
        self.vertices = vertices
        self.indices = indices
        self.d_list = d_list
        self.d_list = d_list
        self.Rreg = np.identity(3)
        self.preg = np.matrix([ [0.0],[0.0],[0.0]] )
  
        self.s = None
        self.s = self.calculate_s(self.d_list, self.Rreg, self.preg)

        # Build OctTree
        self.octTree = OctTree.OctTree(self.vertices, self.indices)
        

    # Iterates through one loop of ICP. Only called on once in 
    # assignment 3
    def iterate(self):
        c_list = []
        distance_list = []
        # for each s, find closest point by calling on OctTrees'
        # find_closest_point() method
        for s in self.s:
            cp, dt = self.octTree.find_closest_point(s)
            c_list.append(cp)
            distance_list.append(dt)
        return c_list, distance_list
            
    # Calculated s_k = R*d + p
    def calculate_s(self, d_list, R, p):
        d_cloud = PointCloud.PointCloud(d_list)
        transformation = Frame_Transformation.Frame_Transformation()
        s_list = transformation.transform(R, p, d_cloud)
        return s_list
        


