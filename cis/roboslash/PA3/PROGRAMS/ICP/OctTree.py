#module OctTree
import sys
import numpy as np
import Sphere_Creator as Sphere_Creator
import Closest_Point as Closest_Point
import Containers.Sphere as Sphere

'''
OctTree class represents an oct tree be used as way to divide the state space. Each node of an
oct tree contains a subspace of the entire state space of the mesh grid. In addition, each node of an
oct tree contains at most 8 children node, each representing a subspace of the parent node's 
space. The subspaces are determined by clustering points relative to the space's absolute centerl.
This purpose of this hierarchial data structure is to continuously filter out choices which are
unlikely, resulting in only a space subspace of the entire system to be measured. Therefore,
all searches are O(log(n)) time.

'''

class OctTree(object):

    '''
    BoundingBodTreeNode is a node class private to OctTree. This class represents a node on the OctTree,
    containing information such as the spheres containing, upper and lower bounds, and space center
    ''' 
    class BoundingBoxTreeNode(object):
        def __init__(self, spheres, nSpheres, min_count=150, min_diag=7.5):
            self.spheres = spheres   # spheres contained in this bound
            self.nSpheres = nSpheres # Number of spheres
            self.center = self.find_centroid(self.spheres, self.nSpheres) # the subspace's center point
            self.MaxRadius = self.find_max_radius(self.spheres) # The highest radius amongst all spheres
            self.ub = self.find_max_coordinates(self.spheres) # The maximum coordinates in the subspace
            self.lb = self.find_min_coordinates(self.spheres) # The minimum coordinates in the space
            self.have_subtrees = 0 # Is a leaf node?
 
            self.subtrees = [] # children
            self.min_count = min_count # minimum number of spheres, any lower and the node is a leaf
            self.min_diag = min_diag # min size of the boundaries, any lower and the node is a leaf
            self.construct_subtrees() # builds the tree

            self.bound = sys.maxint # bound on spheres
            self.closest = None # closest point
            self.min_distance = sys.maxint # minimum distance acquired

            '''
                The next three variables are permanent and used as a reference for its
                non permanent counterparts to go back to. This is useful since we are finding
                closest points to multiple s_k, we need to make sure each node is reset after
                each search, else an incorrect answer may result
            '''
            self.bound_perm = sys.maxint
            self.closest_perm = None
            self.min_distance_perm = sys.maxint
            
        # returns the closest point
        def return_closest_point(self):
            return self.closest
        # returns the minimum distance
        def return_min_distance(self):
            return self.min_distance
        # returns the number of subtrees in the node
        def return_num_subtrees(self):
            return len(self.subtrees)

        # Searchs the tree for closest point to s
        def search(self, s):
            # Each node goes through this method once, therefore it is appropriate to reset
            # bound, closest, and min_distance here
            self.bound = self.bound_perm
            self.closest = self.closest_perm
            self.min_distance = self.min_distance_perm

            # Check to see if point is outside of bound +/- distance
            dist = self.MaxRadius + self.bound
            if s[0,0] > self.ub[0] + dist : return
            if s[1,0] > self.ub[1] + dist : return
            if s[2,0] > self.ub[2] + dist : return
            if s[0,0] < self.lb[0] - dist : return
            if s[1,0] < self.lb[1] - dist : return
            if s[2,0] < self.lb[2] - dist : return

            # If the node has subtrees, go here
            if self.have_subtrees != 0:
                # recursively call on search() for each child
                for tree in self.subtrees:
                    tree.search(s)
                # bring the closest point and minium distance back up, eventually reaching its way to the node
                # consider that a parent node will need to choose which the minimum of the min distances from
                # the points of which its children have chosen
                for tree in self.subtrees:
                    if tree.return_min_distance() < self.min_distance:
                        self.min_distance = tree.return_min_distance()
                        self.closest = tree.return_closest_point()
            # Else, find the closest point
            else:
                # Find distance to each sphere
                for sphere in self.spheres:
                    self.update_closest(sphere, s)

        # Updates self.min_distance, self.bound, self.closest if sphere is closer to else then previously recorded min
        def update_closest(self, sphere, s):
            dist = np.linalg.norm(s - np.transpose(np.asmatrix(sphere.return_centroid())))
            # if outside bound, no change
            if (dist - sphere.return_radius() > self.bound): 
                return

            # find closest point and compare to bound. If less then, update self.closest, self.bound, self.min_distance
            cp = Closest_Point.Closest_Point(s, sphere.return_vertices())
            closest = cp.return_closest_point()
            dist = np.linalg.norm(closest - np.transpose(s))
            if dist < self.bound:
                self.bound = dist
                self.closest = closest
                self.min_distance = dist
            
        # Constructs the tree
        def construct_subtrees(self):
            # If spheres is less than min_count or the bound diagonal is less than min_diag, leaf is node
            if self.nSpheres <= self.min_count or np.linalg.norm(self.ub-self.lb) < self.min_diag:
                self.have_subtrees = 0
                return
            self.have_subtrees = 1
            # acquire "buckets"/split the subspace into further subspaces
            buckets = self.split_sort(self.center, self.spheres)
            for bucket in buckets:
                # each bucket is a child node
                self.subtrees.append(self.__class__(bucket, len(bucket)))

        # Splits a subspace into 8 further subspaces
        def split_sort(self, center, spheres):
            buckets = [ [] for _ in range(8) ]
            # define the center coordinates
            xc, yc, zc = center[0], center[1], center[2]
            # Put sphere into appropriate bucket relative to sphere's centroid
            for sphere in spheres:
                centroid = sphere.return_centroid()
                x, y, z = centroid[0], centroid[1], centroid[2]

                ''' 
                    The following is logic for putting a sphere into its appropraite bucket
                    Using Pascal's Triangle, we know that in 3 dimensions there are 1 + 3 + 3 + 1
                    buckets. One instance where all centroid coordinates are < subspace center coordiantes,
,                   one instance where all centroid coordinates are > subspace center coordinates,
                    3 instances where 2 of the centroid coordinates are < subspace center coordinates,
                    and 3 instances where 1 of the centroid coordinates are < subspace center coordinates
                '''
                if x < xc and y < yc and z < zc:
                    buckets[0].append(sphere)
                elif x < xc and y < yc and z >= zc:
                    buckets[1].append(sphere)
                elif x < xc and y >= yc and z >= zc:
                    buckets[2].append(sphere)
                elif x >= xc and y < yc and z < zc:
                    buckets[3].append(sphere)
                elif x >= xc and y >= yc and z < zc:
                    buckets[4].append(sphere)
                elif x < xc and y >= yc and z < zc:
                    buckets[5].append(sphere)
                elif x >= xc and y < yc and z >= zc:
                    buckets[6].append(sphere)
                else:
                    buckets[7].append(sphere)
            
            return buckets

            
        # Finds the sphere with max radius
        def find_max_radius(self,spheres):
            max_radius = 0.0
            for sphere in spheres:
                rad = sphere.return_radius()
                if rad > max_radius: max_radius = rad
            return max_radius

        # Using the centroid of each sphere, find the absolute center of the subspace
        def find_centroid(self, spheres, nSpheres):
            x_sum, y_sum, z_sum = 0.0, 0.0, 0.0
            total = nSpheres
            if total == 0: return None
            for sphere in spheres:
                sp_centroid = sphere.return_centroid()
                x_sum += sp_centroid[0] 
                y_sum += sp_centroid[1]
                z_sum += sp_centroid[2]
            centroid = np.array([x_sum/total, y_sum/total, z_sum/total])
            return centroid

        # Find the maximum coordinates in the subspace. These coordinates could be a mix from different spheres
        def find_max_coordinates(self, spheres):
            x_max, y_max, z_max = -1*float(sys.maxint) -1, -1*float(sys.maxint) -1, -1*float(sys.maxint) - 1
            for sphere in spheres:
                centroid = sphere.return_centroid()
                x, y, z = centroid[0], centroid[1], centroid[2]
                if x > x_max: x_max = x
                if y > y_max: y_max = y
                if z > z_max: z_max = z
            max_coordinate = np.array([x_max, y_max, z_max])
            return max_coordinate

        # Find the minimum coordinates in the subspace. These coordinates could be a mix from different spheres
        def find_min_coordinates(self, spheres):
            x_min, y_min, z_min = float(sys.maxint), float(sys.maxint), float(sys.maxint)
            for sphere in spheres:
                centroid = sphere.return_centroid()
                x, y, z, = centroid[0], centroid[1], centroid[2]
                if x < x_min: x_min = x
                if y < y_min: y_min = y
                if z < z_min: z_min = z
            min_coordinate = np.array([x_min, y_min, z_min])
            return min_coordinate
           
            

    def __init__(self, vertices, indices):
        self.vertices = vertices
        self.indices = indices

        sr = Sphere_Creator.Sphere_Creator(self.vertices, self.indices)
        spheres = sr.obtain_spheres()
        self.root = self.BoundingBoxTreeNode(spheres, len(spheres))
    def find_closest_point(self, s):
        # point of interest, closest_point, bound, min_distance
        #cp, bound, min_dist = self.root.search(s, None, sys.maxint, sys.maxint)
        self.root.search(s)
        #print cp
        #return cp, min_dist
        return self.root.return_closest_point(), self.root.return_min_distance()

    
