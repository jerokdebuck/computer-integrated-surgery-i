
#module NaiveICP
from Transformation import Frame_Transformation
from Containers import PointCloud
import Closest_Point
import numpy as np
import math
import sys
'''
NaiveICP represnts a naive way of performing the iterative closest point
algorithm. For each sk, a linear search is done through all triangle 
surfaces to find a closest point c. In addition, ICP has not been fully
implemented for this assignment - a full implementation will be seen 
for asssignment 4
'''

class NaiveICP(object):

    def __init__(self, vertices, indices, d_list):
        self.vertices = vertices
        self.indices = indices
        self.d_list = d_list
   
        # Initialize F_reg = I
        self.Rreg = np.identity(3)
        self.preg = np.matrix([ [0.0],[0.0],[0.0]] )
  
        self.s = None
        # Calculate s_k = F_reg * d_k
        self.s = self.calculate_s(self.d_list, self.Rreg, self.preg)


    # Calculates s = R*d + p
    def calculate_s(self, d_list, R, p):
        d_cloud = PointCloud.PointCloud(d_list)
        transformation = Frame_Transformation.Frame_Transformation()
        s_list = transformation.transform(R, p, d_cloud)
        return s_list


    # Iterates through one loop of ICP. For assignment 3, this will
    # only be called on once
    def iterate(self):
        self.min_points = []
        self.min_distances = []
        # for each s, find the closest point
        for s in self.s:
            min_distance = float(sys.maxint)
            min_point = None
            # perform linear search
            for index in self.indices:
                vertex_one, vertex_two, vertex_three = index[0], index[1], index[2]
                p = np.asmatrix(self.vertices[int(vertex_one)])
                q = np.asmatrix(self.vertices[int(vertex_two)])
                r = np.asmatrix(self.vertices[int(vertex_three)])
                # Use Closest_Point class to acquire closest point
                cp = Closest_Point.Closest_Point(s, [p,q,r])
                c = cp.return_closest_point()
                distance = np.linalg.norm(c - np.transpose(s))
                # if distance is less than current min, swap
                if distance < min_distance:
                    min_distance = distance
                    min_point = c
            # append closet point and distance to relevant data structures
            self.min_points.append(min_point)
            self.min_distances.append(min_distance)
        # return list of closest point nad list of shortest distances
        return self.min_points, self.min_distances


