#module Distortion_Calibration
import numpy as np

import Containers.Frame as Frame
import Containers.PointCloud as PointCloud

import Transformation.Frame_Transformation as Frame_Transform

import Registration.Registration as Registration
'''
  ' This class represents a Distortion Calibration s.t. given
  ' measured and reference frames, a transformation can be found
  ' between all pointclouds within the frames. The result is an expected
  ' pointcloud based off known calibration techniques
'''

class Distortion_Calibration(object):

    def __init__(self, body_parser_A, body_parser_B, samples_parser):
        # Acquire A_tip and put into PointCloud class
        self.A_tip = body_parser_A.return_tip()
        tip_Frame = Frame.Frame([ [self.A_tip] ])
        self.tip_cloud = tip_Frame.return_clouds()[0]

        self.body_A_frame = body_parser_A.return_frame()
        self.body_B_frame = body_parser_B.return_frame()
        # Put A body vectors and B body vectors into single frame
        AB_vectors = [self.body_A_frame.return_clouds()[0].return_vectors(), self.body_B_frame.return_clouds()[0].return_vectors()]
        self.body_AB_frame = Frame.Frame(AB_vectors)
        
        self.sample_frames = samples_parser.return_frames()
        # acquire first line information from file
        self.na, self.nb, self.nd, self.nsamps = samples_parser.return_info()
        
        

    # Compute the average and vectors relative to average of cloud
    def cloud_computation(self, clouds):
        for c in clouds:
            c.compute_average()
            c.compute_relative_vectors()
 
    # Perform Distortion Calibration
    def calibrate(self):
        body_clouds = self.body_AB_frame.return_clouds()
        self.cloud_computation(body_clouds)
        body_clouds_list = [body_clouds[i] for i in xrange(0, len(body_clouds))]
        d_expected_list = []
        for frame in self.sample_frames:
            # calculate average and relative vectors for each cloud in each frame
            self.cloud_computation(frame.return_clouds())
            curr_transformation = Frame_Transform.Frame_Transformation()
            clouds = frame.return_clouds()
            # Acquire C to be returned and used as reference for correction fnc
            # For each cloud in frame except last, perform 
            # the registration to acquire (Rd,pd) and (Ra,pa)
            for i in xrange(0, len(clouds) - 1):
                reg = Registration.Registration(clouds[i], body_clouds_list[i])
                rot, trans = reg.registration()
                curr_transformation.add(body_clouds_list[i], clouds[i], (rot, trans))
            # Fd = FB^-1*FA
            (Rd, pd) = curr_transformation.calculate_F(clouds[1], clouds[0])
            # Given Fd, transform points A_tip to d_expec
            d = curr_transformation.transform(Rd, pd, self.tip_cloud)
            d_expected_list.append(d)
        return d_expected_list
        


