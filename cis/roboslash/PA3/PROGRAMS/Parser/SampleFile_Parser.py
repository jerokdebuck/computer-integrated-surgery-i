

#module SampleFile_Parser

import numpy as np
import Containers.Frame as Frame

class SampleFile_Parser(object):

    def __init__(self, f, Na, Nb):
        self.f = f
        self.frames = []
        self.vectors = []
 
        self.Na = Na
        self.Nb = Nb
        self.Nd = None
        self.Ns = None
        self.Nsamps = None

        self.parse()

    def parse(self):
        data_file = open(self.f, "r")
        first_line = data_file.readline().strip().split(",")
        self.Ns, self.Nsamps = int(first_line[0].strip()), int(first_line[1].strip())

        self.Nd = self.Ns - self.Na - self.Nb
  
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0: break
            vector = vector.split(",")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        self.create_frames()

    def create_frames(self):
        frame_points = self.Ns
        for i in xrange(0, self.Nsamps):
            a_vectors = self.vectors[i*frame_points: i*frame_points + self.Na]
            b_vectors = self.vectors[i*frame_points + self.Na: i*frame_points + self.Na + self.Nb]
            d_vectors = self.vectors[i*frame_points + self.Na + self.Nb: i*frame_points + self.Na + self.Nb + self.Nd]
            self.frames.append(Frame.Frame([a_vectors, b_vectors, d_vectors]))
    def return_frames(self):
        return self.frames
    def return_info(self):
        return self.Na, self.Nb, self.Nd, self.Nsamps
