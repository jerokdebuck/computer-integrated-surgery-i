
#module BodyFile_Parser

import numpy as np
import Containers.Frame as Frame

class BodyFile_Parser(object):

    def __init__(self, f):
        self.f = f
        self.frame = None
        self.vectors = []
        self.tip = None
        self.N = None

        self.parse()

    def parse(self):
        data_file = open(self.f, "r")
        first_line = data_file.readline().strip().split(" ")
        self.N = int(first_line[0].strip())
        
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0: break
            vector = " ".join(vector.split())
            vector = vector.split(" ")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        data_file.close()
        self.create_frame()

    def create_frame(self):
        body_vectors = self.vectors[0:self.N]
        self.tip = self.vectors[-1]
        self.frame = Frame.Frame([body_vectors])
        
    def return_frame(self):
        return self.frame
    def return_number_markers(self):
        return self.N

    def return_tip(self):
        return self.tip

