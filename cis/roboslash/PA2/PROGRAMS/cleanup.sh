#!/usr/bin/env bash

find . -type d -print0 | while IFS= read -r -d '' dir
do
    rm $dir/*.pyc
done
