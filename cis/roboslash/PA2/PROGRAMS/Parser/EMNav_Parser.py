#module EMNav_Parser
import numpy as np
import sys
sys.path.insert(0, 'Containers')
import Frame

class EMNav_Parser(object):

    def __init__(self, f):
        self.f = f
        self.frames = []
        self.vectors = []
        self.parse()

    def parse(self):

        data_file = open(self.f, "r")
        # get the vector amounts out of the first line
        first_line = data_file.readline().strip().split(",")
        self.ng, self.nf = int(first_line[0].strip()), int(first_line[1].strip())
        # read the sets of vectors out of the file, turn them into numpy_arrays, which
        # are vector like
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0:
                 break
            vector = vector.split(",")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        data_file.close()
        self.create_frames()

    def create_frames(self):
        # each frame contains a set of D_i, A_i, and C_i vectors
        frame_points = self.ng
        # load each frame
        for i in xrange(0, self.nf):
            g_vectors = self.vectors[i*frame_points: i*frame_points + self.ng]
            self.frames.append(Frame.Frame([g_vectors]))

    def return_frames(self):
        return self.frames
    def return_frame_info(self):
        return self.nf
