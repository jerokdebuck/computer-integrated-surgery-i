
#module CTFid_Parser
import numpy as np
import sys
sys.path.insert(0, 'Containers')
import Frame

class CTFid_Parser(object):

    def __init__(self, f):
        self.f = f
        self.vectors = []
        self.frame = None
        self.parse()

    def parse(self):

        data_file = open(self.f, "r")
        # get the vector amounts out of the first line
        first_line = data_file.readline().strip().split(",")
        self.nb = int(first_line[0].strip())
        # read the sets of vectors out of the file, turn them into numpy_arrays, which
        # are vector like
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0:
                 break
            vector = vector.split(",")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        data_file.close()
        self.create_frame()


    def create_frame(self):
        b_vectors = self.vectors[0:self.nb]
        # load each frame
        self.frame = Frame.Frame([b_vectors])

    def return_frame(self):
        return self.frame
