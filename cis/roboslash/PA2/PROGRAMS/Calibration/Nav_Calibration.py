
#module Nav_Calibration
import numpy as np
import Parser.EMNav_Parser as EMNav

import Containers.Frame as Frame
import Containers.PointCloud as PointCloud

import Transformation.Frame_Transformation as Frame_Transform

import Registration.Registration as Registration


class Nav_Calibration(object):

    def __init__(self, nav_name):
        self.nav_name = nav_name
        self.nav_frames = []
        self.nf = self.parse()

    def cloud_computation(self, clouds):
        for c in clouds:
            c.compute_average()
            c.compute_relative_vectors()
    # Parse in data 
    def parse(self):
        nav_parser = EMNav.EMNav_Parser(self.nav_name)
        self.nav_frames = nav_parser.return_frames()
        return nav_parser.return_frame_info()
   
    # Find P_dimp of tracker tip with respect to EM tracker
    # Use g_cloud calculated from EMP Pivot Calibration as reference
    # For each frame find F[k] s.t. G = F[k]*g, then use P_dimp = F*t_g
    def tip_points(self, t_g, g_cloud):
        P_points = []
        #g_cloud = self.reference_frame(self.nav_frames[0].return_clouds()[0])
        for frame in self.nav_frames:
            #g_cloud = self.reference_frame(frame.return_clouds()[0])
            G_cloud = frame.return_clouds()[0]
            self.cloud_computation([G_cloud])
            reg = Registration.Registration(G_cloud, g_cloud)
            (R, p) = reg.registration()
            P_points.append(R*t_g + p)
        return P_points, self.nf

    # Using F_reg, find P_dimp points with respect to CT coordinates
    def find_CT_points(self, P_points, R, pr):
        fixed_p_points = []
        for p in P_points:
            fixed_p_points.append(np.array([p[0,0], p[1,0], p[2,0]]))
        p_cloud = PointCloud.PointCloud(fixed_p_points)
        self.cloud_computation([p_cloud])
        transform = Frame_Transform.Frame_Transformation()
        ct_points = transform.transform(R, pr, p_cloud)
        return ct_points
 
    # Return nav frames
    def return_frames(self):
        return self.nav_frames
    # Clear nav frames
    def clear_frames(self):
        self.nav_frames = []
    # Add a nav frame
    def add_frame(self, points):
        fr = Frame.Frame([points])
        self.nav_frames.append(fr)
