
#module Opt_Calibration
import numpy as np
import Parser.OPTPivot_Parser as OPTPivot

import Containers.Frame as Frame
import Containers.PointCloud as PointCloud

import Transformation.Frame_Transformation as Frame_Transform

import Registration.Registration as Registration

class Opt_Calibration(object):

    def __init__(self, opt_name, d_cloud):
        self.opt_name = opt_name
        self.d_cloud = d_cloud
        self.parse()

    def cloud_computation(self, clouds):
        for c in clouds:
            c.compute_average()
            c.compute_relative_vectors()

    def parse(self):
        opt_parser = OPTPivot.OPTPivot_Parser(self.opt_name)
        self.opt_frames = opt_parser.return_frames()

 
    def calibrate(self):
        # Essentially the same as EM_Calibration except
        # Points in optical tracker space are converted to
        # em tracker space via (R_d, p_d)
        h_cloud = self.reference_frame(self.opt_frames[0].return_clouds()[1])
        R_list, p_list = [], []
        for frame in self.opt_frames:
            D_cloud = frame.return_clouds()[0]
            self.cloud_computation([D_cloud])
            d_reg = Registration.Registration(D_cloud, self.d_cloud)
            (R_d, p_d) = d_reg.registration()
            H_cloud = frame.return_clouds()[1]
            self.cloud_computation([H_cloud])
         
            EM_H_cloud = self.opt_to_emp(R_d, p_d, H_cloud)
            EM_h_cloud = self.opt_to_emp(R_d, p_d, h_cloud)
            h_reg = Registration.Registration(EM_H_cloud, EM_h_cloud)
            (R_h, p_h) = h_reg.registration()
            R_list.append(R_h)
            p_list.append(p_h)
        A_curr, p_curr = self.least_squares_matrix_organization(R_list, p_list)
        x = np.linalg.lstsq(A_curr, p_curr)
        return x[0][3:,0]

    def opt_to_emp(self, R, p, X):
        opt_em_transformation = Frame_Transform.Frame_Transformation()
        em_x = opt_em_transformation.transform_inverse(R, p, X)
        em_x_vecs = []
        for x in em_x:
            x_array = np.array([x[0,0], x[1,0], x[2,0]])
            em_x_vecs.append(x_array)
        x_cloud = PointCloud.PointCloud(em_x_vecs)
        self.cloud_computation([x_cloud])
        return x_cloud
        

    def reference_frame(self, X_cloud):
        self.cloud_computation([X_cloud])
        x_cloud = PointCloud.PointCloud(X_cloud.return_vectors_relative_average())
        self.cloud_computation([x_cloud])
        return x_cloud

    def least_squares_matrix_organization(self, R_list, p_list):
        A_matrices = []
        p_vectors = []

        for R in R_list:
            idt = np.matrix([ [-1, 0, 0], [0, -1, 0], [0, 0, -1] ])
            A = np.concatenate((R, idt), axis=1)
            A_matrices.append(A)

        for p in p_list:
            p = np.matrix([ [-1*p[0,0], -1*p[1,0], -1*p[2,0]] ])
            p_vectors.append(p)

        A_curr = A_matrices[0]
        for A in A_matrices[1:]:
            A_curr = np.concatenate((A_curr, A))
        p_curr = np.transpose(-1*p_vectors[0])
        for p in p_vectors[1:]:
            p_curr = np.concatenate((p_curr, np.transpose(-1*p)))
        return A_curr, p_curr
