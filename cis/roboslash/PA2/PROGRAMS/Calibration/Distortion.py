#module Distortion
import numpy as np
import scipy
import math


class Distortion(object):
    def __init__(self):
        # For finding 5th degree bernstein polynomials
        self.degree = 5
    def bernstein_prelude(self, p, q):
        u = []
        # For each vector in q
        for qr in q:
            #u1 = u in x_dimension, u2 = u in y_dimension, u3 = u in z_dimension
            u1 = self.scaleToBox(qr[0,0], self.q_min_1, self.q_max_1)
            u2 = self.scaleToBox(qr[1,0], self.q_min_2, self.q_max_2)
            u3 = self.scaleToBox(qr[2,0], self.q_min_3, self.q_max_3)
            u.append(np.matrix([ [u1, u2, u3] ]) )
     
        # u will be a list containing len(q) vectors, where each vector
        # is 3 dimensional
    
        # When we are finding correction function, this will be ran
        if p != None:
            # Create a p_matrix where each row is a a point
            # and each column is a dimension (x,y,z)
            # P_matrix will be (len(p), 3)
            p_matrix = np.matrix([ [p[0][0,0], p[0][1,0], p[0][2,0]] ])
            for pk in p[1:]:
                p_matrix = np.concatenate( (p_matrix, np.matrix([ [pk[0,0], pk[1,0], pk[2,0]] ])) )
            return p_matrix, u
        return None, u
        
    def correction_function(self, p, q):
        # First, acquire the maximum and minimum
        # Points for each dimension. This will used as a 
        # standard when calculating bernstein polynomials
        # of all future dewarping instances
        self.q_min_1 = self.find_min(q,1)
        self.q_max_1 = self.find_max(q,1)
 
        self.q_min_2 = self.find_min(q,2)
        self.q_max_2 = self.find_max(q,2)
 
        self.q_min_3 = self.find_min(q,3)
        self.q_max_3 = self.find_max(q,3)
 
        # Acquire P matrix, a standardized matrix
        p_matrix, u = self.bernstein_prelude(p,q)
        # Using the standardized matrix, solve acquire Bernstein matrix
        b_matrix = self.bernstein_matrix(u, self.degree)
        # Use least squares to solve for coefficient matrix
        # i.e. solve for C in BC=P
        coefficient_matrix = np.linalg.lstsq(b_matrix, p_matrix)

        return coefficient_matrix[0]

    def undistort(self, q, coefficient_matrix):
        # Acquire standardized matrix
        garbage, u = self.bernstein_prelude(None, q)
        # Acquire bernstein matrix
        b_matrix = self.bernstein_matrix(u, self.degree)
        #print b_matrix
        # Solve for p matrix using B*C = P
        # B = (len(q), 6^3) C = (6^3,3), P = (len(q),3)
        # where P is just the dewarped points of q
        return b_matrix * coefficient_matrix
        
        
    def bernstein_matrix(self, u, degree):
        # Bernstein matrix will have len(u) rows
        # and 6^3 columns
        total_bernstein_matrix = np.empty(shape=(len(u), (degree+1)**3))
        tally = 0
        # iterate through all vectors in u
        for ur in u:
            curr_burr = []
            # Loop through all possible permutations of 000 -> 555
            for i in xrange(0, degree+1):
                bernstein_x = self.bernstein(ur[0,0],i,degree)
                for j in xrange(0, degree+1):
                    bernstein_y = self.bernstein(ur[0,1],j,degree)
                    for k in xrange(0, degree+1):
                        bernstein_z = self.bernstein(ur[0,2],k,degree)
                        #F(i,j,k) = B(i)*B(j)*B(k)
                        f_ijk = bernstein_x * bernstein_y * bernstein_z
                        curr_burr.append(f_ijk)
            # After all permutations acquired, curr_burr will constitute
            # as a row in the bernstein matrix, where tally decides
            # which row number
            burr_matrix = np.asmatrix(np.array(curr_burr))
            total_bernstein_matrix[tally] = burr_matrix
            tally += 1
        return total_bernstein_matrix

    def bernstein(self, u, k, N):
        #(N k) * u^(1-k) * v^k
        N_choose_k = scipy.math.factorial(N)/(scipy.math.factorial(k)*scipy.math.factorial(N-k))
        v = 1 - u
        x = N-k
        return N_choose_k*(u**x)*(v**k)
        
                   
    def scaleToBox(self, qr, minimum, maximum):
        # Scale point to box governed by a min and max
        u = (qr - minimum) / (maximum - minimum)
        return u

    def find_min(self, q, pos):
        # find the minimum point in a dimension
        vals = [] 
        for qr in q:
            vals.append(qr[(pos-1),0])
        minimum = None
        for i in xrange(0,len(vals)):
            if vals[i] < minimum or minimum == None:
                minimum = vals[i]
        return minimum

    def find_max(self, q, pos):
        # find the maximum point in a dimension
        vals = []
        for qr in q:
            vals.append(qr[(pos-1),0])
        maximum = None
        for i in xrange(0,len(vals)):
            if vals[i] > maximum or maximum == None:
                maximum = vals[i]
        return maximum
        
