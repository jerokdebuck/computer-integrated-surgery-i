-----------------------------------------
Darian Hadjiabadi // dhadjia1@gmail.com
Timothy Mullen // tmullen2@jhu.edu
600.445, Computer Integrated Surgery I
Programming Assignment 4
-----------------------------------------

HOW TO RUN:
    
    This assignment was implemented using Python with extensive use of
    the numpy module. In the PROGRAMS/ directory you will find an
    executable 'main.py'. To run the program, perform the following

        $ ./main [max_iteratations] < input.txt
                         OR 
        $ python main.py [max_iterations] < input.txt

    max_iterations is an optional command line argument that allows the user
    to control how many iterations of ICP will be run before
    halting. With our implementation, it takes worst case 10 minutes
    to reach convergence at 38 iterations. Therefore if the
    max_iterations is to be used, it should be < 38 (for example,
    19-20 iterations will take 5 minutes). If max_iterations is 
    not used, the default is set to 150.

    The main program has been tested on the ugrad cluster and should
    work on any machine as long as the python intrepeter is in
    /usr/bin. If not, the shebang line must be changed to include
    the location of the intrepeter.

    The program will output a single file in the same directory as main.py 
    (PROGRAMS/). The file will contain the output information as requested in the assignment
   
    NOTE: 

    unit testing can be turned on/off by uncommenting/commenting the
    first line and second lines in main().

DIRECTORY HIERARCHY AND FILE DESCRIPTION:

DATA/ - Directory containing all input data

OUTPUT/ - Directory containing the output

PROGRAMS/ - Directory containing all source files
|
|
|---> cleanup.sh - Remove *.pyc files
|---> main.py - The driver python file to run the assignment
                Produces solutions in the form as described in pdf
|---> input.txt - File containing a list of input files (first four) and 
                  output files (last one).
                  User should change change line 3 & 5 as needed to run all Samples
                  inputs and output its corresponding .txt file. input.txt is initially
                  set to handle the 'A' sample data.
|
|
---> Containers/ - Directory containing container classes for the assignment : Frame, PointCloud
      |
      |
      ---> Frame.py - Contains a simple Frame class to hold PointClouds
      ---> PointCloud.py - Contains PointCloud class which contains vectors
           in a given space as well as the average vector and its vectors
           relative to the average. 
      ---> Sphere.py - A bounded sphere based off a surface
|
---> Registration/ - Directory containing Registration class
     |
     |
     ---> Registration.py - A registration class that implements Horns method
|
---> Transforamtion/ - Directory containing Transformation class
     |
     |
     ---> Frame_Transformation.py - A transformation class that transforms
          one PointCloud to another given a rotation matrix and 
          translation vector
|
---> Parser/ - Directory containing various parser classes to parse input files
     |
     |---> BodyFile_Parser.py - Class that parses BodyX.txt files
     |---> MeshFile_Parser.py - Class that parses MeshFile.sur files
     |---> SampleFile_Parser.py - Class that parses SampleReadingsTest.txt files
|
---> Calibration/ - Directory containing various Calibration classes and a class for dewarping
      |
      |---> Distortion_Calibration.py - Class to produce Fb^-1*Fa and to calculate A_tip relative to body B frame
|
---> ICP/ - Directory containing algorithms for iterative closest point as well as necessary advanced data structures
      |
      |---> Closest_Point.py - An algorithm to finding closest point
      |---> AdvancedICP.py - ICP algorithm implementing oct trees for search
      |---> OctTree.py - An implementation of oct tree
            contains inner class BoundingBoxTreeNode
      |---> Sphere_Creator.py - Creates bounding spheres for the state space
    
