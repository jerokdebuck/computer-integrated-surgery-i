
#module MeshFile_Parser

import numpy as np
import Containers.Frame as Frame

class MeshFile_Parser(object):

    def __init__(self, f):
        self.f = f
        self.num_vertices = None
        self.num_triangles = None
        self.vertices = []
        self.vertex_indices = []

        self.parse()

    def parse(self):
        data_file = open(self.f, "r")
        first_line = data_file.readline().strip().split(" ")
        self.num_vertices = int(first_line[0].strip())
        for i in xrange(0, self.num_vertices):
            line = data_file.readline().strip().split(" ")
            vertex_pts = np.array([float(line[0].strip()), float(line[1].strip()), float(line[2].strip())] )
            self.vertices.append(vertex_pts)
        line = data_file.readline().strip().split(" ")
        self.num_triangles = int(line[0].strip())
        for i in xrange(0, self.num_triangles):
            line = data_file.readline().strip().split(" ")
            vertex_index = np.array([float(line[0].strip()), float(line[1].strip()), float(line[2].strip())] )
            self.vertex_indices.append(vertex_index)
        data_file.close()

    def return_vertices(self):
        return self.vertices
    def return_indices(self):
        return self.vertex_indices

