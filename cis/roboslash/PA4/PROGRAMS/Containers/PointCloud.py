'''
  ' Darian Habjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' An abstraction for a cluster of points within 3d space
'''
#module PointCloud
import numpy as np

# A class containing a list of vectors
# the average point of the vectors
# and a list of vectors relative to the average

class PointCloud(object):

    def __init__(self, vectors):
        self.vectors = vectors
        self.average_vector = np.zeros(3)
        self.relative_average_vectors = []

    # Compute the average of the vectors
    def compute_average(self):
        x_tot, y_tot, z_tot = 0.0, 0.0, 0.0
        for vector in self.vectors:
            x_tot += vector[0] 
            y_tot += vector[1]
            z_tot += vector[2]
        totals = (x_tot, y_tot, z_tot)
        for i in xrange(0, len(totals)):
            self.average_vector[i] = totals[i] / len(self.vectors)

    def compute_relative_vectors(self):
        # Compute the vectors relative to the average
        self.relative_average_vectors = []
        for vector in self.vectors:
            rel_vec = np.zeros(3)
            for i in xrange(0, 3):
                rel_vec[i] = vector[i] - self.average_vector[i]
            self.relative_average_vectors.append(rel_vec)
    def return_average(self):
        return self.average_vector 
    def return_vectors(self):
        return self.vectors
    def return_vectors_relative_average(self):
        return self.relative_average_vectors
