#module Sphere
'''
  ' An abstraction for a bounding sphere to be used with ICP
  ' Sphere contains a centroid, radius, and list of triangle vertices
  ' off which the centroid is based off of
'''

class Sphere(object):
    def __init__(self, centroid, radius, vertices):
        self.centroid = centroid
        self.radius = radius
        self.vertices = vertices

    def return_centroid(self):
        return self.centroid
    def return_radius(self):
        return self.radius
    def return_vertices(self):
        return self.vertices
