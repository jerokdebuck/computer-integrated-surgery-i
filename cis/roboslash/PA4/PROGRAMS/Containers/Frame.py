'''
  ' Darian Hadjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' Class file for the Frame data construct we use to hold point clouds
  ' , where pointclouds are abstractions for a cloud of vectors in 3D
  ' 
'''
#module Frame
import numpy as np
import PointCloud

class Frame(object):
    def __init__(self, clouds):
        self.point_clouds = []
        # put clouds in list data strcuture
        for i in xrange(0, len(clouds)):
            self.point_clouds.append(PointCloud.PointCloud(clouds[i]))
    def return_clouds(self):
        return self.point_clouds
        

