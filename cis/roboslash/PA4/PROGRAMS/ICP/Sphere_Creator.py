#module Sphere_Creator
import numpy as np
import Containers.Sphere as Sphere

''' 
    For each triangle a sphere can be bounded. This class builds
    a data structure to contain all spheres in the state space.
    Each element in the data structure is a Sphere object with
    radius, centroid, and triangle on which the sphere maps to
'''

class Sphere_Creator(object):

    def __init__(self, vertices, indices):
        self.vertices = vertices
        self.indices = indices
    # obtain all spheres in the space
    def obtain_spheres(self):
        spheres = []
        # For each triangle acquire its bounding sphere
        for index in self.indices:
            p, q, r = self.vertices[int(index[0])], self.vertices[int(index[1])], self.vertices[int(index[2])]
            # find max segment
            a, b, c, max_dist = self.find_longest_segment(p, q, r)
            f = (a+b)/2
            u = a - f
            v = c - f
            d1 = np.cross(u,v)
            d = np.cross(d1, u)
            # if lamb = 0 then centroid is simply a+b/2
            lamb = self.scale(d, u, v)
            centroid = f + (lamb*d)
            #print np.dot( (centroid - a), (centroid - a) ), np.dot( (centroid -b), (centroid - b) )
            # radius = ||centoid - a||
            radius = np.linalg.norm(centroid - a)
            # add a sphere object to spheres list
            spheres.append(Sphere.Sphere(centroid, radius, [p,q,r]))
        return spheres

    # Finds the longest segment in triangle
    def find_longest_segment(self, p, q, r):
        pq = np.linalg.norm(p-q)
        pr = np.linalg.norm(p-r)
        qr = np.linalg.norm(q-r)
        max_dist = max(pq, pr, qr)
        if max_dist == pq: return p, q, r, max_dist
        elif max_dist == pr: return p, r, q, max_dist
        else: return q, r, p, max_dist
 
    # Determines if the centroid falls in the middle of the longest
    # segment or if it needs to be scaled
    def scale(self, d, u, v):
        num = np.dot(u, u) - np.dot(v, v)
        denom = np.dot(2*d, v-u)
        gamma = num / denom
        if gamma <= 0: return 0
        return gamma
