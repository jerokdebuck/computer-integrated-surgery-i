
#module AdvancedICP
import OctTree as OctTree
from Transformation import Frame_Transformation
from Registration import Registration
from Containers import PointCloud
import numpy as np
import math
import sys
import time
'''
AdvancedICP represnts a more advanced way of performing iterative closest
point algorithm. An oct tree is built to split the entire point space
into hierarchiac fragments, which each node contains a subet of the
space based on certain bounds. AdvancedICP builds this tree and simply
finds the closest point for all s_k of interest
'''

class AdvancedICP(object):

    def __init__(self, vertices, indices, d_list):

        self.c_list = []
        self.distance_list = []

        self.vertices = vertices
        self.indices = indices
        self.d_list = d_list
        self.Rreg = np.identity(3)
        self.preg = np.matrix([ [0.0],[0.0],[0.0]] )
  
        self.s = None

        # Build OctTree
        self.octTree = OctTree.OctTree(self.vertices, self.indices)

    ''' Runs the ICP algorithm until convergence or max iterations has been reached
        Convergence occurs in the following:
        1) Maximum number of iterations reached
        2) Change in error below .001
        3) For 15 iterations the difference in errors is below .0005
    '''
    def run_icp(self, max_iter):
        # d_cloud will be used to update F_reg
        d_cloud = PointCloud.PointCloud(np.array(self.d_list))
        d_cloud.compute_average()
        d_cloud.compute_relative_vectors()

        convergence = 0.001 # What the error must go under to converge
        min_difference = 0.0005 # The minimum change in error
        prev_error = 0.0
        prev_delta_error = 0.0

        # keeps track of number of iterations
        iteration_count = 0
        # keeps track of consecutive small changes in error
        min_count = 0
        while True:
            print 'iteration {0}'.format(iteration_count+1)
            iteration_count += 1
            self.s = self.calculate_s(self.d_list, self.Rreg, self.preg)
            # Find the error, c_k and distances for all s_k
            error, self.c_list, self.distance_list = self.iterate()
            # find the change in error
            delta_error = math.fabs(prev_error - error)
            print 'delta error: {0}'.format(delta_error)
            # find if difference in change of errors is significant
            if math.fabs(delta_error - prev_delta_error) < min_difference:
                # if small changes occur 15 times consecutively, halt
                if min_count == 15:
                    print 'Minimum reached, halting icp'
                    return self.s, self.c_list, self.distance_list
                else: 
                    print 'Little change will halt after {0} more iteration'.format(15 - min_count - 1)
                    min_count += 1
            else:
                min_count = 0
            prev_delta_error = delta_error
            # check for convergence or maximum iterations
            if delta_error < convergence or iteration_count == max_iter:
                print 'Full convergence met / iteration count exceeded'
                return self.s, self.c_list, self.distance_list
            else:
                print 'did not meet criteria for convergence'
            prev_error = error
            c_array = []
            for c in self.c_list:
                c_array.append([c[0,0], c[0,1], c[0,2]])
            c_cloud = PointCloud.PointCloud(np.array(c_array))
            c_cloud.compute_average()
            c_cloud.compute_relative_vectors()
            # update F_reg
            self.Rreg, self.preg = self.update_transformation(c_cloud, self.Rreg, self.preg, d_cloud)
        

    # Updates F_reg through use of point cloud to point cloud registration
    def update_transformation(self, c, R, p, d):
        reg = Registration.Registration(c, d)
        new_R, new_p = reg.registration()
        return new_R, new_p
        
    # Iterates through one loop of ICP.
    def iterate(self):
        error = None
        c_list = []
        distance_list = []
        # for each s, find closest point by calling on OctTrees'
        # find_closest_point() method
        for sk in self.s:
            cp, dt = self.octTree.find_closest_point(sk)
            c_list.append(cp)
            distance_list.append(dt)

        #error = sum(distance_list)/len(distance_list)
        error = max(distance_list)
        return error, c_list, distance_list
            
    # Calculated s_k = R*d + p
    def calculate_s(self, d_list, R, p):
        d_cloud = PointCloud.PointCloud(d_list)
        transformation = Frame_Transformation.Frame_Transformation()
        s_list = transformation.transform(R, p, d_cloud)
        return s_list
        

