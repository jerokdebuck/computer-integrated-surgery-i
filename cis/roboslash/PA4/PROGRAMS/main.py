#!/usr/bin/python
'''
  ' Darian Hadjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' Driver Program for Assignment 3
  ' 
  ' 
'''
import sys
import time
import numpy as np

# Import Calibration tool
import Calibration.Distortion_Calibration as Distortion_Calibration

# import file parsers
import Parser.BodyFile_Parser as BodyFile_Parser
import Parser.SampleFile_Parser as SampleFile_Parser
import Parser.MeshFile_Parser as MeshFile_Parser

#import ICP tools
import ICP.AdvancedICP as AdvancedICP

# main function
def main():

    # comment or uncomment the next two lines to perform unit tests
    #unit_testing()
    #sys.exit(1)

    input_location = "../DATA/"
    body_name_A = input_location + sys.stdin.readline().strip()
    body_name_B = input_location + sys.stdin.readline().strip()

    # Parse the BodyFile information for body A
    body_parser_A = BodyFile_Parser.BodyFile_Parser(body_name_A)
    Na = body_parser_A.return_number_markers()
 
    # Parse the BodyFile inforation for body B
    body_parser_B = BodyFile_Parser.BodyFile_Parser(body_name_B)
    Nb = body_parser_B.return_number_markers() 

    # Parse the sample input data
    sample_readings_name = input_location + sys.stdin.readline().strip()
    sample_parser = SampleFile_Parser.SampleFile_Parser(sample_readings_name, Na, Nb)

    # Perform distortion calibration to acquire d_expected
    # s.t. d_expected = Fb^-1*Fa*A_tip
    # where d_expected is the location of the tip of tool A relative
    # to tool B (which is stuck in place)
    distort_calibration = Distortion_Calibration.Distortion_Calibration(body_parser_A, body_parser_B, sample_parser)
    d_expected = distort_calibration.calibrate() 
    d = []
    for d_list in d_expected:
        for dl in d_list:
            d.append(np.array( [dl[0,0], dl[1,0], dl[2,0]] ) )

    # Parse in the mesh information
    mesh_name = input_location + sys.stdin.readline().strip()
    mesh_parser = MeshFile_Parser.MeshFile_Parser(mesh_name)

    max_iterations = 150
    if len(sys.argv) == 2:
        max_iterations = int(sys.argv[1])
        print max_iterations
    run_advanced(mesh_parser.return_vertices(), mesh_parser.return_indices(), d, sample_parser.return_info()[3], max_iterations)


# function for running advaned ICP via oct trees and bounding spheres
def run_advanced(vertices, indices, d, nsamps, max_iterations):
    advanced = AdvancedICP.AdvancedICP(vertices, indices, d)
    print 'Starting ICP' 
    start_time = time.clock()
    s_ct, c_ct, distance_differences = advanced.run_icp(max_iterations)
    total_time = time.clock() - start_time
    print 'ICP completed in {0} seconds'.format(total_time)
    output_file =  sys.stdin.readline().strip()
    print_output(output_file, s_ct, c_ct, distance_differences, nsamps)
    

# fucntion to print output
def print_output(output, d, c_ct, distance_differences, nsamps):
    f = open(output, "w")
    f.write(str(nsamps) + "\t" + output + "\n")
    for i in xrange(0, len(c_ct)):
        f.write("%8s %8s %8s" % (str(round(d[i][0],2)), str(round(d[i][1], 2)), str(round(d[i][2], 2)) ) )
        f.write("\t")
        f.write("%8s %8s %8s" % (str(round(c_ct[i][0,0], 2)), str(round(c_ct[i][0,1], 2)), str(round(c_ct[i][0,2], 2)) ) )
        f.write("\t")
        f.write("%8s" % (str(round(distance_differences[i], 3)) ) )
        f.write('\n')   
    f.close()

# perform unit test
import ICP.Sphere_Creator as Sphere_Creator
def unit_testing():

    # This method creates three triangles and 3 points of interest
    # initially the points are closest points on the triangle
    # distorted points are created by adding +/- 0.1 to each
    # space component. This test unit will aim to realign the distorted
    # points back to the original closest points as defined by 
    # true points
    triangles = [np.array([3.0, 4.0, 5.0]), np.array([10.0, 12.0, 15.0]), np.array([-2.0, -3.0, -4.0]), np.array([3.0, 4.0, -1.0]), np.array([10.0, 12.0, -2.0]), np.array([-2.0,-3.0, 1.0]), np.array([-1.0,4.0,5.0]), np.array([-1.0,12.0,15.0]), np.array([1.0, -3.0, -4.0]) ]
    index = [np.array([0,1,2]), np.array([3,4,5]), np.array([6,7,8])]
    true_points = [np.array([3.0,4.0,5.0]), np.array([10.0, 12.0, 15.0]), np.array([10.0,12.0,-2.0])]
    distorted = [np.array([3.1, 4.1, 5.1]), np.array([10.1,12.1,15.1]), np.array([10.1,12.1,-2.1])]
    icp = AdvancedICP.AdvancedICP(triangles, index, distorted)
    s_ct, c_ct, distance_differences = icp.run_icp(30)
    print distance_differences
    print '===================='
    print s_ct
    print '===================='
    print c_ct
    print '===================='


    
if __name__ == '__main__':
    main()
