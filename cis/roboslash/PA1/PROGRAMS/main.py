#!/usr/bin/python
'''
  ' Darian Habjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' Driver Program for Assignment 1 to produce solutions for 4 through 6
  '
  ' 
'''
import sys
import numpy as np
# Import the parsers
import Parser.Calbody_Parser as Calbody
import Parser.Calreading_Parser as Calread
import Parser.EMPivot_Parser as EMPivot
import Parser.OPTPivot_Parser as OPTPivot
# Import the position frame tools
import Containers.Frame as Frame
import Containers.PointCloud as PointCloud
# Import Registration tool
import Registration.Registration as Registration
# Import Transformation tool
import Transformation.Frame_Transformation as Frame_Transform

def main():
    body_name = "../DATA/" + sys.stdin.readline().strip()
    reading_name = "../DATA/" + sys.stdin.readline().strip()
    # Part 4, find C_expected. Also return the vectors in d
    # to be used in part 6 for finding F_d
    c_expected, d_cloud, nc, nf = distortion_calibration(body_name, reading_name)

    emp_name = "../DATA/" + sys.stdin.readline().strip()
    # Part 5, find P_dimp
    em_P_dimp = em_calibration(emp_name)

    opt_name = "../DATA/" + sys.stdin.readline().strip()
    opt_P_dimp = opt_calibration(opt_name, d_cloud)


    # Print results
    first_line = str(nc) + ', ' + str(nf) + ', ' + str(sys.stdin.readline())
    sys.stdout.write(first_line)
    print_vectors([-1*em_P_dimp], 3)
    print_vectors([-1*opt_P_dimp], 3)
    for c in c_expected:
        print_vectors(c, 3)

def print_vectors(c, dims):

    for transed in c:
        transed = np.transpose(transed)
        for i in xrange(0, dims):
            if i == (dims - 1):
                sys.stdout.write(str(round(transed[0,i], 2)))
            else:
                sys.stdout.write(str(round(transed[0,i], 2)) + ',' +'\t\t')
        sys.stdout.write('\n')


def cloud_computation(clouds):
    # Compute the average vector
    # and vectors relative to the average
    # for each cloud in the clouds list
    for cloud in clouds:
        cloud.compute_average()
        cloud.compute_relative_vectors()


def distortion_calibration(body_name, reading_name):
    '''
      ' Return the expected position readings C_expected of the EM tracker
      ' given known information about the Calibration Body and a set of concurrent
      ' readings of the Body from both trackers 
      '
      ' d_i vectors are position vectors of the optical markers on the EM tracker base,
      '   relative to the base
      ' a_i vectors are position vectors of the optical markers on the Calibration
      '  Body relative to the Body
      ' c_i vectors are position vectors of the EM markers on the Calibration Body
      '  relative to the Body
      '
      ' D_i vectors are readings of the optical markers on the EM tracker base,
      '   relative to the optical tracker
      ' A_i vectors are readings of the optical markers on the Calibration
      '  Body relative to the optical tracker
      ' C_i vectors are position vectors of the EM markers on the Calibration Body
      '  relative to the EM_tracker
    '''
    # Parse the body file and acquire its fixed frame
    body_parser = Calbody.Calbody_Parser(body_name)
    body_frame = body_parser.return_frame()
    # Acquire all the clouds (there will be 3 -> d, a, c)
    body_clouds = body_frame.return_clouds()
    cloud_computation(body_clouds)
    body_cloud_list = [body_clouds[0], body_clouds[1], body_clouds[2]]

    # Parse the reading file and acquire its frames.
    # Note return_frames() will return a list of frames, each of which
    # will have a list of clouds
    reading_parser = Calread.Calreading_Parser(reading_name)
    reading_frames = reading_parser.return_frames()
    nc, nf = reading_parser.return_frame_info()
    c_expected_list = []
    for frame in reading_frames:
        # Calculate average and relative vectors for each cloud
        # in each frame
        cloud_computation(frame.return_clouds())
        curr_transformation = Frame_Transform.Frame_Transformation() 
        clouds = frame.return_clouds()
        # For each cloud in the frame except for the last, perform
        # the registration to acquire (Rd,pd) and (Ra,pa)
        for i in xrange(0, len(clouds) - 1):
          reg = Registration.Registration(clouds[i], body_cloud_list[i])
          rot, trans = reg.registration()
          curr_transformation.add(body_cloud_list[i], clouds[i], (rot, trans))
        # Acquire Rc using Frame_Transformation object
        # Note Fc = Fd^-1 * Fa
        (Rc, pc) = curr_transformation.calculate_F(clouds[0], clouds[1])
        # Given Fc, transform points c to C_expec (i.e. C_expec = Fc * c)
        C = curr_transformation.transform(Rc, pc, body_cloud_list[2])
        c_expected_list.append(C)
    return c_expected_list, body_clouds[0], nc, nf

def em_calibration(emp_name):
    ''' 
      ' Return the point P_dimp using pivot calibration technique
      ' For each frame, F_G[k] is computed using a reference cloud
      ' g and EM tracker points G[k]. After computing each transformation
      ' P_dimp is acquired via least squared method
    '''
    # Read in emp_name and acquire its frames
    emp_parser = EMPivot.EMPivot_Parser(emp_name)
    emp_frames = emp_parser.return_frames()
    # Use the first frame as overall reference frame 
    # G_mean = sum(G_j) / N
    # g_j = G_j - G_mean
    g_cloud = reference_frame(emp_frames[0].return_clouds()[0])
    R_list, p_list = [], []
    for frame in emp_frames:
        # Get the cloud from frame (Note there is only one cloud in each
        # of the empivot frames
        G_cloud = frame.return_clouds()[0]
        cloud_computation([G_cloud])
        # Acquire R and p for each frame relative to g_cloud
        reg = Registration.Registration(G_cloud, g_cloud)
        (R, p) = reg.registration()
        R_list.append(R)
        p_list.append(p)
    A_curr, p_curr = least_squares_matrix_organization(R_list, p_list)
    # Perform least squares, retrieving t_g and P_dimp
    # Return P_dimp
    x = np.linalg.lstsq(A_curr, p_curr)
    return x[0][3:,0]


def opt_calibration(opt_name, d_cloud):
    '''
      ' Return the point P_dimp relative to the tracker base
      ' using pivot calibration technique. Similiar to em_calibration
      ' except points in optical tracker space are transformed to em tracker
      ' space. P_dimp is acquired via a least squared method
    '''
     
    # Essentially the same as em_calibration except
    # Points in optical tracker space are converted to
    # em tracker space via (R_d, p_d)
    opt_parser = OPTPivot.OPTPivot_Parser(opt_name)
    opt_frames = opt_parser.return_frames()
    h_cloud = reference_frame(opt_frames[0].return_clouds()[1])
    R_h_list, p_h_list = [], []
    for frame in opt_frames:
        # Find R_d, p_d
        D_cloud = frame.return_clouds()[0]
        cloud_computation([D_cloud])
        d_reg = Registration.Registration(D_cloud, d_cloud)
        (R_d, p_d) = d_reg.registration()
        H_cloud = frame.return_clouds()[1]
        cloud_computation([H_cloud])
        # Acquire the transformed points
        EM_H_cloud = opt_to_emp(R_d, p_d, H_cloud)
        EM_h_cloud = opt_to_emp(R_d, p_d, h_cloud)
        
        h_reg = Registration.Registration(EM_H_cloud, EM_h_cloud)
        (R_h, p_h) = h_reg.registration()
        R_h_list.append(R_h)
        p_h_list.append(p_h)
    A_curr, p_curr = least_squares_matrix_organization(R_h_list, p_h_list)
    x = np.linalg.lstsq(A_curr, p_curr)
    return x[0][3:,0]

def opt_to_emp(R, p, X):
    # Convert points in optical tracker to points in emp tracker frame
    opt_em_transformation = Frame_Transform.Frame_Transformation()
    em_x = opt_em_transformation.transform_inverse(R, p, X)
    em_x_vecs = []
    for x in em_x:
        x_array = np.array([x[0,0], x[1,0], x[2,0]])
        em_x_vecs.append(x_array)
    x_cloud = PointCloud.PointCloud(em_x_vecs)
    cloud_computation([x_cloud])
    return x_cloud

def reference_frame(X_cloud):
    # Acquire a reference frame for calibration
    cloud_computation([X_cloud])
    x_cloud = PointCloud.PointCloud(X_cloud.return_vectors_relative_average())
    cloud_computation([x_cloud])
    return x_cloud

def least_squares_matrix_organization(R_list, p_list):
    A_matrices = []
    p_vectors= []

    # Least squares matrix takes on form
    # [  .. | ..               [ ..
    #   R_k | -I * [ t_g     =  p_k
    #    .. | ..      --         ..
    #          ]     P_dimp]    ]
    for R in R_list:
        # concatenate R with -I for each frame 
        # i.e. create [R | -I]
        idt = np.matrix([ [-1, 0, 0], [0,-1,0], [0,0,-1] ])
        A = np.concatenate((R, idt),axis=1)
        A_matrices.append(A)

    for p in p_list:
        # negative p_k
        p = np.matrix([ [-1*p[0,0], -1*p[1,0], -1*p[2,0]] ])
        p_vectors.append(p)

    # For each A, concatenate so that the length is 
    # equal to #frames * 3
    A_curr = A_matrices[0]
    for A in A_matrices[1:]:
        A_curr = np.concatenate((A_curr, A))

    # Do the same with p
    p_curr = np.transpose(-1*p_vectors[0])
    for p in p_vectors[1:]:
        p_curr = np.concatenate((p_curr, np.transpose(-1*p)))
    return A_curr, p_curr  

if __name__ == '__main__':
    main()
