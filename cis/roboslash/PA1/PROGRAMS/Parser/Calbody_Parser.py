'''
  ' Darian Habjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' Reads in "NAME-CALBODY.TXT" files, which describe the Calibration Body
  ' and the optical markers on the EM tracker base.
  '
  ' The files contain position vectors of the optical and EM markers on the
  ' Calibration Body relative to the Calibration Body, and position vectors
  ' of the optical markers on the EM tracker base (relative to the base).
  '
  ' d_i vectors are position vectors of the optical markers on the EM tracker base,
  '   relative to the base
  ' a_i vectors are position vectors of the optical markers on the Calibration
  '  Body relative to the Body
  ' c_i vectors are position vectors of the EM markers on the Calibration Body
  '  relative to the Body
  '
  ' These vectors are loaded into a "Frame" data structure for usage
'''
#module Calbody_Parser

import numpy as np
import sys
import Containers.Frame as Frame

class Calbody_Parser(object):
    '''
      ' A Parser object reads in the vectors, given in 3D Cartesian components
    '''
    def __init__(self, f):
        self.f = f
        self.vectors = []
        self.parse()

    def parse(self):
        '''
          ' The first line is formatted as "N_D, N_A, N_C, NAME-calbody.txt"
          ' where N_D, N_A, and N_C are integers and NAME is the file prefix
          ' N_D = number of optical markers on EM tracker base(d_i vectors)
          ' N_A = number of optical markers on Calibration Body (a_i vectors)
          ' N_C = number of EM markers on Calibration Body (c_i vectors)
          ' 
          ' The first N_D # of lines (after the first line) are the d_i vectors
          ' followed by N_A a_i vectors, and N_C c_i vectors
          '
          ' Lines are formatted as "?_xi, ?_yi, ?_zi" which are x, y, and z
          ' integer components of the i'th ? vector respectively
        '''        

        data_file = open(self.f, "r")
        # get the vector amounts out of the first line
        first_line = data_file.readline().strip().split(",")
        self.nd, self.na, self.nc = int(first_line[0].strip()), int(first_line[1].strip()), int(first_line[2].strip())
        # read the sets of vectors out of the file, turn them into numpy_arrays, which
        # are vector like
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0:
                 break
            vector = vector.split(",")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        data_file.close()
        self.create_frame()

    def create_frame(self):
        d_vectors = self.vectors[0:self.nd]
        a_vectors = self.vectors[self.nd:self.nd+self.na]
        c_vectors = self.vectors[self.nd+self.na:self.nd+self.na+self.nc]
        # Create global frame with calibration vector sets
        self.frame = Frame.Frame([d_vectors, a_vectors, c_vectors])

    def return_frame(self):
        return self.frame
