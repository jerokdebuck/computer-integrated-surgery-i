'''
  ' Darian Habjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' Info
'''
#module OPTPivot_Parser
import numpy as np
import sys
sys.path.insert(0, 'Containers')
import Frame

class OPTPivot_Parser(object):

    def __init__(self, f):
        self.f = f
        self.frames = []
        self.vectors = []
        self.parse()
    def parse(self):
        data_file = open(self.f, "r")
        first_line = data_file.readline().strip().split(",")
        self.nd, self.nh, self.nf = int(first_line[0].strip()), int(first_line[1].strip()), int(first_line[2].strip())
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0:
                 break
            vector = vector.split(",")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        data_file.close()
        self.create_frames()

    def create_frames(self):
        frame_points = self.nd + self.nh
        for i in xrange(0, self.nf):
            D_vectors = self.vectors[i*frame_points: i*frame_points + self.nd]
            H_vectors = self.vectors[i*frame_points + self.nd: i*frame_points + self.nd + self.nh]
            self.frames.append(Frame.Frame([D_vectors, H_vectors]))

    def return_frames(self):
        return self.frames
