import numpy as np
import sys
sys.path.insert(0, 'Containers')
import PointCloud

# A class that performs registration given a PointCloud x and PointCloud y
class Registration(object):

    def __init__(self, y, x):
        self.y = y
        self.x = x
    def registration(self):
        # Perform the registration based on the Horn Method
        y_avg = self.y.return_average()
        y_relative_average = self.y.return_vectors_relative_average()
        x_avg = self.x.return_average()
        x_relative_average = self.x.return_vectors_relative_average()
        rotation_matrix = self.rotation_via_horn(x_relative_average, y_relative_average)
        x_avg_matrix = np.matrix([ [x_avg[0], x_avg[1], x_avg[2]] ])
        y_avg_matrix = np.matrix([ [y_avg[0], y_avg[1], y_avg[2]] ])
        translation = np.transpose(y_avg_matrix) - rotation_matrix*np.transpose(x_avg_matrix)
        return rotation_matrix, translation

    def rotation_via_horn(self, x, y):
        H_matrix = np.zeros( (3,3) )
        assert len(x) == len(y)
        # Acquire the H matrix
        for i in xrange(0, len(x)):
            curr_x, curr_y = x[i], y[i]
            for j in xrange(0, len(curr_x)):
                for k in xrange(0, len(curr_x)):
                    H_matrix[j][k] = H_matrix[j][k] + curr_x[j]*curr_y[k]
        # Compute G matrix
        G = self.compute_G_matrix(H_matrix)
        # Acquire eigenvalues and eigenvectors
        eigenvalues, eigenvectors = np.linalg.eig(G)
        # Find largest eigenvalue and return vector for that value
        max_val, max_pos = -1e10, -1e10
        for i in xrange(0, len(eigenvalues)):
            if eigenvalues[i] > max_val:
                max_val = eigenvalues[i]
                max_pos = i
        unit_quat = eigenvectors[:,max_pos]
        return self.compute_rotation_matrix(unit_quat)

    def compute_G_matrix(self,H):
        # Logic for acquiring G matrix
        trace = np.trace(H)
        delta_transpose = np.array([H[1][2] - H[2][1], H[2][0] - H[0][2], H[0][1]- H[1][0]])
        delta = np.transpose(delta_transpose)
        g_corner = H + np.transpose(H) - (trace*np.identity(3))
        # Putting it all togther
        G = np.matrix([ [trace, delta_transpose[0], delta_transpose[1], delta_transpose[2]], [delta[0], g_corner[0][0], g_corner[0][1], g_corner[0][2]], [delta[1], g_corner[1][0], g_corner[1][1], g_corner[1][2]], [delta[2], g_corner[2][0], g_corner[2][1], g_corner[2][2]] ])
        return G
        
    def compute_rotation_matrix(self, unit_quat):
        q0, q1, q2, q3 = float(unit_quat[0]), float(unit_quat[1]), float(unit_quat[2]), float(unit_quat[3])
        # Logic for turning unit quaternion to rotation matrix
        r1c1 = q0**2 + q1**2 - q2**2 - q3**2
        r1c2 = 2*(q1*q2 - q0*q3)
        r1c3 = 2*(q1*q3 + q0*q2)
         
        r2c1 = 2*(q1*q2 + q0*q3)
        r2c2 = q0**2 - q1**2 + q2**2 - q3**2
        r2c3 = 2*(q2*q3 - q0*q1)

        r3c1 = 2*(q1*q3 - q0*q2)
        r3c2 = 2*(q2*q3 + q0*q1)
        r3c3 = q0**2 - q1**2 - q2**2 + q3**2


        Rotation = np.matrix([ [r1c1, r1c2, r1c3], [r2c1, r2c2, r2c3], [r3c1, r3c2, r3c3] ])
        return Rotation
         

         
  
        
 
        
