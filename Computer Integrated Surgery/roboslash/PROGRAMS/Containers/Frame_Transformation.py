import numpy as np
import PointCloud

# A class that allows for mappings of points in y
# to its Transformation (R,p) and points in x.
# Also allows for calculation of F1^1 * F2 as well
# as performing transformations given R,p and x to find y = Rx + p

class Frame_Transformation(object):

    def __init__(self):
        self.coordinate_transformation = {}
    # Add a transformation from x to y given A = (R,p)
    def add(self, x, y, A):
        self.coordinate_transformation[y] = [A,x]
    # Calculate F1^-1*F2
    def calculate_F(self,c1,c2):
       # t_1 = (R1,p1)
       t_1 = self.coordinate_transformation[c1][0]
       # t_2 = (R2,p2)
       t_2 = self.coordinate_transformation[c2][0]
       # Rc = R_1^-1 * R_2
       Rc = np.linalg.inv(t_1[0])*t_2[0]
       # pc = R_1^-1*p_2 - R_1^-1*p_1
       pc = np.linalg.inv(t_1[0])*t_2[1] - np.linalg.inv(t_1[0])*t_1[1]
       return (Rc, pc)
    def transform(self, R, p, x):
        C = []
        for v in x.return_vectors():
            # Create a numpy matrix from the array
            vec = np.matrix([[v[0], v[1], v[2]] ])
            # C = Rx + p
            C.append(R*np.transpose(vec) + p)
        A = (R,p)
        # add the transform to the mapping for future use (if need be)
        C_cloud = PointCloud.PointCloud(C)
        C_cloud.compute_average()
        C_cloud.compute_relative_vectors()
        self.coordinate_transformation[C_cloud] = [A, x]
        return C
     

            
