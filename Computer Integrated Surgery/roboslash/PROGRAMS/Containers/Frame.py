import numpy as np
import PointCloud

# A simple Frame class that contains multiple points clouds
# Allows for easy access to each point cloud in the frame
class Frame(object):
    def __init__(self, clouds):
        self.point_clouds = []
        for i in xrange(0, len(clouds)):
            self.point_clouds.append(PointCloud.PointCloud(clouds[i]))
    def return_clouds(self):
        return self.point_clouds
        

