#!/usr/bin/python

import sys
import numpy as np
# Import the parsers from Parser directory
sys.path.insert(0, 'Parser')
import Calbody_Parser
import Calreading_Parser
import EMPivot_Parser
import OPTPivot_Parser
sys.path.insert(0, 'Containers')
import Frame
import Frame_Transformation
import PointCloud
sys.path.insert(0, 'Registration')
import Registration

def print_vectors(c):
    for transed in c:
        transed = np.transpose(transed)
        for i in xrange(0, 3):
            sys.stdout.write(str(round(transed[0,i], 2)) + ',' +'\t')
        sys.stdout.write('\n')


def cloud_computation(clouds):
    # Compute the average vector
    # and vectors relative to the average
    # for each cloud in the clouds list
    for cloud in clouds:
        cloud.compute_average()
        cloud.compute_relative_vectors()


def distortion_calibration(body_name, reading_name):
    # Parse the body file and acquire its frame
    body_parser = Calbody_Parser.Calbody_Parser(body_name)
    body_frame = body_parser.return_frame()
    # Acquire all the clouds (there will be 3 -> d, a, c)
    body_clouds = body_frame.return_clouds()
    cloud_computation(body_clouds)
    body_cloud_list = [body_clouds[0], body_clouds[1], body_clouds[2]]

    # Parse the reading file and acquire its frames.
    # Note return_frames() will return a list of frames, each of which
    # will have a list of clouds
    reading_parser = Calreading_Parser.Calreading_Parser(reading_name)
    reading_frames = reading_parser.return_frames()
    c_expected_list = []
    for frame in reading_frames:
        # Calculate average and relative vectors for each cloud
        # in each frame
        cloud_computation(frame.return_clouds())
        curr_transformation = Frame_Transformation.Frame_Transformation() 
        clouds = frame.return_clouds()
        # For each cloud in the frame except for the last, perform
        # the registration to acquire (Rd,pd) and (Ra,pa)
        for i in xrange(0, len(clouds) - 1):
          reg = Registration.Registration(clouds[i], body_cloud_list[i])
          rot, trans = reg.registration()
          curr_transformation.add(body_cloud_list[i], clouds[i], (rot, trans))
        # Acquire Rc using Frame_Transformation object
        # Note Fc = Fd^-1 * Fa
        (Rc, pc) = curr_transformation.calculate_F(clouds[0], clouds[1])
        # Given Fc, transform points c to C_expec (i.e. C_expec = Fc * c)
        C = curr_transformation.transform(Rc, pc, body_cloud_list[2])
        c_expected_list.append(C)
    return c_expected_list

def em_calibration(emp_name):
    # Read in emp_name and acquire its frames
    emp_parser = EMPivot_Parser.EMPivot_Parser(emp_name)
    emp_frames = emp_parser.return_frames()
    # Use the first frame as overall reference frame 
    temp_system = PointCloud.PointCloud(emp_frames[0].return_clouds()[0].return_vectors())
    cloud_computation([temp_system])
    # G_mean = sum(G_j) / N
    # g_j = G_j - G_mean
    g_cloud = PointCloud.PointCloud(temp_system.return_vectors_relative_average())
    cloud_computation([g_cloud])
    R_list = []
    p_list = []
    for frame in emp_frames:
        # Get the cloud from frame (Note there is only one cloud in each
        # of the empivot frames
        G_cloud = frame.return_clouds()[0]
        cloud_computation([G_cloud])
        # Acquire R and p for each frame relative to g_cloud
        reg = Registration.Registration(G_cloud, g_cloud)
        (R, p) = reg.registration()
        R_list.append(R)
        p_list.append(p)
    A_curr, p_curr least_squares_matrix_organization(R_list, p_list)
    # Perform least squares, retrieving t_g and P_dimp
    # Return P_dimp
    x = np.linalg.lstsq(A_curr, p_curr)
    return x[0][3:,0]

def least_squares_matrix_organization(R_list, p_list):
    A_matrices = []
    p_vectors= []

    # Least squares matrix takes on form
    # [  .. | ..               [ ..
    #   R_k | -I * [ t_g     =  p_k
    #    .. | ..      --         ..
    #          ]     P_dimp]    ]
    for R in R_list:
        # concatenate R with -I for each frame 
        # i.e. create [R | -I]
        idt = np.matrix([ [-1, 0, 0], [0,-1,0], [0,0,-1] ])
        A = np.concatenate((R, idt),axis=1)
        A_matrices.append(A)

    for p in p_list:
        # negative p_k
        p = np.matrix([ [-1*p[0,0], -1*p[1,0], -1*p[2,0]] ])
        p_vectors.append(p)

    # For each A, concatenate so that the length is 
    # equal to #frames * 3
    A_curr = A_matrices[0]
    for A in A_matrices[1:]:
        A_curr = np.concatenate((A_curr, A))

    # Do the same with p
    p_curr = np.transpose(-1*p_vectors[0])
    for p in p_vectors[1:]:
        p_curr = np.concatenate((p_curr, np.transpose(-1*p)))
    return A_curr, p_curr  

body_name = "../DATA/" + sys.stdin.readline().strip()
reading_name = "../DATA/" + sys.stdin.readline().strip()
# Part 4, find C_expected
c_expected = distortion_calibration(body_name, reading_name)

emp_name = "../DATA/" + sys.stdin.readline().strip()
# Part 5, find P_dimp
em_P_dimp = em_calibration(emp_name)


# Print results
print_vectors([-1*em_P_dimp])
for c in c_expected:
    print_vectors(c)
