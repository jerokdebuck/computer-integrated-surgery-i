import numpy as np
import sys
sys.path.insert(0, 'Containers')
import Frame

class Calbody_Parser(object):

    def __init__(self, f):
        self.f = f
        self.vectors = []
        self.parse()
    def parse(self):
        data_file = open(self.f, "r")
        first_line = data_file.readline().strip().split(",")
        self.nd, self.na, self.nc = int(first_line[0].strip()), int(first_line[1].strip()), int(first_line[2].strip())
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0:
                 break
            vector = vector.split(",")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        data_file.close()
        self.create_frame()

    def create_frame(self):
        d_vectors = self.vectors[0:self.nd]
        a_vectors = self.vectors[self.nd:self.nd+self.na]
        c_vectors = self.vectors[self.nd+self.na:self.nd+self.na+self.nc]
        self.frame = Frame.Frame([d_vectors, a_vectors, c_vectors])

    def return_frame(self):
        return self.frame
