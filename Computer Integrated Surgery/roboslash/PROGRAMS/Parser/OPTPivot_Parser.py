import numpy as np

class OPTPivot_Parser(object):

    def __init__(self, f):
        self.f = f
        self.vectors = []
        self.parse()
    def parse(self):
        data_file = open(self.f, "r")
        first_line = data_file.readline().strip().split(",")
        self.nd, self.nh, self.nf = int(first_line[0].strip()), int(first_line[1].strip()), int(first_line[2].strip())
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0:
                 break
            vector = vector.split(",")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        data_file.close()

    def return_markers(self):
        return (self.nd, self.nh, self.nf)
    def return_vectors(self):
        return self.vectors
