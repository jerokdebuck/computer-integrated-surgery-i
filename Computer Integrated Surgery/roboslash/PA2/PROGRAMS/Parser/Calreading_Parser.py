'''
  ' Darian Habjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' Reads in "NAME-CALREADINGS.TXT" files, which contain multiple frames of
  ' optical and EM tracker calibration readings of the markers on the Calibration
  ' Body and EM tracker base
  '
  ' D_i vectors are readings of the optical markers on the EM tracker base,
  '   relative to the optical tracker
  ' A_i vectors are readings of the optical markers on the Calibration
  '  Body relative to the optical tracker
  ' C_i vectors are position vectors of the EM markers on the Calibration Body
  '  relative to the EM_tracker
  '
  ' These vectors are grouped and loaded into one Frame data structure per reading
  ' frame
'''
#module Calreading_Parser
import numpy as np
import sys
sys.path.insert(0, 'Containers')
import Frame

class Calreading_Parser(object):

    def __init__(self, f):
        self.f = f
        self.frames = []
        self.vectors = []
        self.parse()

    def parse(self):
        '''
          ' The first line is formatted as "N_D, N_A, N_C, N_f NAME-calreadings.txt"
          ' where N_D, N_A, N_C and N_f are integers and NAME is the file prefix
          ' N_D = number of optical markers on EM tracker base
          ' N_A = number of optical markers on Calibration Body
          ' N_C = number of EM markers on Calibration Body
          ' N_f = number of frames in the file, each frame containing equal sized
          '       sets of D_i, A_i, and C_i vectors
          ' 
          ' The first N_D # of lines (after the first line) are the D_i vectors
          ' followed by N_A A_i vectors, and N_C C_i vectors, of the first frame
          '
          ' The next frame follows with N_D + N_A + N_C lines
          '
          ' Lines are formatted as "?_xi, ?_yi, ?_zi" which are x, y, and z
          ' integer components of the i'th ?-vector respectively
          '
        '''

        data_file = open(self.f, "r")
        # get the vector amounts out of the first line
        first_line = data_file.readline().strip().split(",")
        self.nd, self.na, self.nc, self.nf = (int(first_line[0].strip()),
                                              int(first_line[1].strip()),
                                              int(first_line[2].strip()),
                                              int(first_line[3].strip()))
        # read the sets of vectors out of the file, turn them into numpy_arrays, which
        # are vector like
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0:
                 break
            vector = vector.split(",")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        data_file.close()
        self.create_frames()


    def create_frames(self):
        # each frame contains a set of D_i, A_i, and C_i vectors
        frame_points = self.nd + self.na + self.nc
        # load each frame
        for i in xrange(0, self.nf):
            d_vectors = self.vectors[i*frame_points: i*frame_points + self.nd]
            a_vectors = self.vectors[i*frame_points + self.nd: i*frame_points + self.na+self.nd]
            c_vectors = self.vectors[i*frame_points + self.nd + self.na: i*frame_points + self.na + self.nd + self.nc]
            self.frames.append(Frame.Frame([d_vectors, a_vectors, c_vectors]))

    def return_frames(self):
        return self.frames
    def return_frame_info(self):
        return self.nc, self.nf
