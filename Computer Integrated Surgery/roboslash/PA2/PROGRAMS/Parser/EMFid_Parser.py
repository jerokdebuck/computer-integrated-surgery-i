'''
  ' Darian Habjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' Reads in "NAME-CALBODY.TXT" files, which describe the Calibration Body
  ' and the optical markers on the EM tracker base.
  '
  ' The files contain position vectors of the optical and EM markers on the
  ' Calibration Body relative to the Calibration Body, and position vectors
  ' of the optical markers on the EM tracker base (relative to the base).
  '
  ' d_i vectors are position vectors of the optical markers on the EM tracker base,
  '   relative to the base
  ' a_i vectors are position vectors of the optical markers on the Calibration
  '  Body relative to the Body
  ' c_i vectors are position vectors of the EM markers on the Calibration Body
  '  relative to the Body
  '
  ' These vectors are loaded into a "Frame" data structure for usage
'''
#module EMFid_Parser

import numpy as np
import sys
import Containers.Frame as Frame

class EMFid_Parser(object):
    '''
      ' A Parser object reads in the vectors, given in 3D Cartesian components
    '''
    def __init__(self, f):
        self.f = f
        self.frames = []
        self.vectors = []
        self.parse()

    def parse(self):

        data_file = open(self.f, "r")
        # get the vector amounts out of the first line
        first_line = data_file.readline().strip().split(",")
        self.ng, self.nf = int(first_line[0].strip()), int(first_line[1].strip())
        # read the sets of vectors out of the file, turn them into numpy_arrays, which
        # are vector like
        while True:
            vector = data_file.readline().strip()
            if len(vector) == 0:
                 break
            vector = vector.split(",")
            np_vector = np.array([float(vector[0].strip()), float(vector[1].strip()), float(vector[2].strip())])
            self.vectors.append(np_vector)
        data_file.close()
        self.create_frame()

    def create_frame(self):
        frame_points = self.ng
        for i in xrange(0, self.nf):
            g_vectors = self.vectors[i*frame_points:i*frame_points+self.ng]
        # Create global frame with calibration vector sets
            self.frames.append(Frame.Frame([g_vectors]))

    def return_frames(self):
        return self.frames
