'''
  ' Darian Habjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' Class file for the Frame data construct we use to hold the data
  '
  ' A frame contains some number of point clouds.  A point cloud is a 
'''
#module Frame
import numpy as np
import PointCloud

# A simple Frame class that contains multiple point clouds
# Allows for easy access to each point cloud in the frame
class Frame(object):
    def __init__(self, clouds):
        self.point_clouds = []
        for i in xrange(0, len(clouds)):
            self.point_clouds.append(PointCloud.PointCloud(clouds[i]))
    def return_clouds(self):
        return self.point_clouds
        

