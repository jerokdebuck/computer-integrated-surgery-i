#!/usr/bin/python
'''
  ' Darian Hadjiabadi and Timothy Mullen
  ' Created for 600.445, Computer Integrated Surgery I
  '
  ' Driver Program for Assignment 2
  '
  ' 
'''
import sys
import numpy as np
# Import Calibration & Dewarping tools
import Calibration.Distortion_Calibration as Distortion_Calibration
import Calibration.EM_Calibration as EM_Calibration
import Calibration.Opt_Calibration as Opt_Calibration
import Calibration.Fid_Calibration as Fid_Calibration
import Calibration.Nav_Calibration as Nav_Calibration
import Calibration.Distortion as Distortion

def main():
    # a unit test of correction function and dewarp, uncomment to run 
    #unit_testing()

    # Acquire C_expec and C
    body_name = "../DATA/" + sys.stdin.readline().strip()
    reading_name = "../DATA/" + sys.stdin.readline().strip()
    d_calibration = Distortion_Calibration.Distortion_Calibration(body_name, reading_name)
    c_expected, c, d_cloud, nc, nf = d_calibration.calibrate()
    # Concatenate all frames into one big list for both c & c_expec
    c_expected_total, c_total = [], []
    for i in xrange(0, len(c)):
        for j in xrange(0, len(c[i])):
            c_expected_total.append(c_expected[i][j])
            c_total.append(c[i][j]) 
    
    # Acquire correction function and dewarp C values
    # note Distortion obect distort used throughout entire
    # assignment to find correction function and dewarp EM space
    distort = Distortion.Distortion()
    coefficient_matrix = distort.correction_function(c_expected_total, c_total)
    new_c = distort.undistort(c_total, coefficient_matrix)

    # Find unwarped P_dimp and t_g using EM calibration technique
    emp_name = "../DATA/" + sys.stdin.readline().strip()
    em_cal = EM_Calibration.EM_Calibration(emp_name) 
    # Dewarp em frames
    '''em_frames = em_cal.return_frames()
    em_cal.clear_frames()
    for frame in em_frames:
        new_vectors = frame_to_distortion(frame)
        # dewarped vectors, call on function to put in a data structure
        # that distort can use
        corrected = distort.undistort(new_vectors, coefficient_matrix)
        corrected_arrays = distortion_to_frame(corrected)
        em_cal.add_frame(corrected_arrays)
    # Acquire P_dimp and t_g from EM pivot calibratio
    # Note that t_g is fixed
    '''
    em_P_dimp, t_g, g_cloud = em_cal.calibrate()
    t_g = -1 * t_g

    # Acquire F_reg from fiducial data
    em_fid_name = "../DATA/" + sys.stdin.readline().strip()
    ct_fid_name = "../DATA/" + sys.stdin.readline().strip()
    fid_cal = Fid_Calibration.Fid_Calibration(em_fid_name, ct_fid_name)
    fidem_frames = fid_cal.return_em_frames()
    fid_cal.clear_em_frames()
    for frame in fidem_frames:
        new_vectors = frame_to_distortion(frame)
        corrected = distort.undistort(new_vectors, coefficient_matrix)
        corrected_arrays = distortion_to_frame(corrected)
        fid_cal.add_frame(corrected_arrays)        
    # Acquire P_dimp points for fiducials
    P_points = fid_cal.tip_points(t_g, g_cloud)
    # Acquire F_reg based on this P_dimp points
    (R_reg, p_reg) = fid_cal.find_F_reg(P_points) 
    
    # Acquire probe position in CT coordinates
    nav_name = "../DATA/" + sys.stdin.readline().strip()
    nav_cal = Nav_Calibration.Nav_Calibration(nav_name)
    nav_frames = nav_cal.return_frames()
    nav_cal.clear_frames()
    for frame in nav_frames:
        new_vectors = frame_to_distortion(frame)
        corrected = distort.undistort(new_vectors, coefficient_matrix)
        corrected_arrays = distortion_to_frame(corrected)
        nav_cal.add_frame(corrected_arrays)
    # Using g_cloud from EM pivot calibration, find new P_dimp points
    # when navigating through space
    Pnav_points, nf2 = nav_cal.tip_points(t_g, g_cloud)
    # Using F_reg, find P_dimp points relative to CT coordinates
    ct_points = nav_cal.find_CT_points(Pnav_points, R_reg, p_reg)
     
    # Acquire P_dimp relative to optical tracker
    opt_name = "../DATA/" + sys.stdin.readline().strip()
    op_cal = Opt_Calibration.Opt_Calibration(opt_name, d_cloud)
    opt_P_dimp = op_cal.calibrate()
    
    # Print results for output of part 1
    output_name = sys.stdin.readline().strip()
    f1  = open(output_name, "w")
    f1.write(str(nc) + ', ' + str(nf) + ', ' + output_name + '\n')
    print_output_1([-1*em_P_dimp], 3, f1)
    print_output_1([-1*opt_P_dimp], 3, f1)
    print_output_2(new_c, 3, f1)
    f1.close()

    # Print results for output of part 2
    output2_name = sys.stdin.readline().strip()
    f2 = open(output2_name, "w")
    f2.write(str(nf2) + ', ' + output2_name + '\n')
    print_output_1(ct_points, 3, f2)
    f2.close()

def print_output_1(c, dims, f):
    for transed in c:
        transed = np.transpose(transed)
        for i in xrange(0, dims):
            if i == (dims - 1):
                f.write(str(round(transed[0,i], 2)))
            else:
                f.write(str(round(transed[0,i], 2)) + ',' +'\t\t')
        f.write('\n')

def print_output_2(c, dims, f):
    for transed in c:
        transed = np.transpose(transed)
        for i in xrange(0, dims):
            if i == (dims - 1):
                f.write(str(round(transed[i,0],2)))
            else:
                f.write(str(round(transed[i,0],2)) + ',' + '\t\t')
        f.write('\n')

# Put vectors from Frame objects into matrix form
def frame_to_distortion(frame):
    vectors = frame.return_clouds()[0].return_vectors()
    new_vectors = []
    for vec in vectors:
        new_vectors.append(np.transpose(np.asmatrix(vec)))
    return new_vectors 

# Put matrix rows acquired from dewarping space into arrays that
# a Frame can use
def distortion_to_frame(corrected):
    arrays = []
    for row in corrected:
        arrays.append(np.array([row[0,0],row[0,1],row[0,2]]))
    return arrays

def unit_testing():

    # Perform test where there is no distortion
    p_1 = np.matrix([ [1.0], [2.0], [3.0] ])
    p = [p_1]
    
    q_1 = np.matrix([ [1.0], [2.0], [3.0] ])
    q = [q_1]

    distort = Distortion.Distortion()
    coefficient_matrix = distort.correction_function(p,q)

    test_x = np.matrix([ [5.0], [10.0], [15.0] ])
    test = [test_x]
    corrected = distort.undistort(test, coefficient_matrix)
    print coefficient_matrix
    print 'dewarped'
    print corrected
    print 'original'
    print test

    # Coefficient matrix should have first row as the position p_1/q_1
    # dewarped should return nan as berstein matrix is all infinity

if __name__ == '__main__':
    main()
