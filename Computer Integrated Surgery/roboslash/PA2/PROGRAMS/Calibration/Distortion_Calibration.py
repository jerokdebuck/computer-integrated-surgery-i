


#module Distortion_Calibration
import numpy as np
import Parser.Calbody_Parser as Calbody
import Parser.Calreading_Parser as Calreading

import Containers.Frame as Frame
import Containers.PointCloud as PointCloud

import Transformation.Frame_Transformation as Frame_Transform

import Registration.Registration as Registration

class Distortion_Calibration(object):

    def __init__(self, body_name, reading_name):
        self.body_name = body_name
        self.reading_name = reading_name
        # nc, nf information for output file
        self.nc, self.nf = self.parse()

    # Compute the average and vectors relative to average of cloud
    def cloud_computation(self, clouds):
        for c in clouds:
            c.compute_average()
            c.compute_relative_vectors()

    # Read in data from files
    def parse(self):
        body_parser = Calbody.Calbody_Parser(self.body_name)
        self.body_frame = body_parser.return_frame()
        body_clouds = self.body_frame.return_clouds()
        self.cloud_computation(body_clouds)
        self.body_clouds_list = [body_clouds[i] for i in xrange(0, len(body_clouds))]
        
        reading_parser = Calreading.Calreading_Parser(self.reading_name)
        self.reading_frames = reading_parser.return_frames()
        nc, nf = reading_parser.return_frame_info()
        return nc, nf
 
    # Perform Distortion Calibration
    def calibrate(self):
        c_list = []
        c_expected_list = []
        for frame in self.reading_frames:
            # calculate average and relative vectors for each cloud in each frame
            self.cloud_computation(frame.return_clouds())
            curr_transformation = Frame_Transform.Frame_Transformation()
            clouds = frame.return_clouds()
            # Acquire C to be returned and used as reference for correction fnc
            c = clouds[2].return_vectors()
            c_matrices = []
            for cp in c:
                new_c = np.transpose(np.asmatrix(cp))
                c_matrices.append(new_c)
            c_list.append(c_matrices)
            # For each cloud in frame except last, perform 
            # the registration to acquire (Rd,pd) and (Ra,pa)
            for i in xrange(0, len(clouds) - 1):
                reg = Registration.Registration(clouds[i], self.body_clouds_list[i])
                rot, trans = reg.registration()
                curr_transformation.add(self.body_clouds_list[i], clouds[i], (rot, trans))
            # Fc = Fd^-1*Fa
            (Rc, pc) = curr_transformation.calculate_F(clouds[0], clouds[1])
            # Given Fc, transform points c to C_expec (i.e. C_expec = Fc * c)
            C = curr_transformation.transform(Rc, pc, self.body_clouds_list[2])
            c_expected_list.append(C)
        return c_expected_list, c_list, self.body_clouds_list[0], self.nc, self.nf
        


