
#module Fid_Calibration
import numpy as np
import Parser.EMFid_Parser as EMFid
import Parser.CTFid_Parser as CTFid

import Containers.Frame as Frame
import Containers.PointCloud as PointCloud

import Transformation.Frame_Transformation as Frame_Transform

import Registration.Registration as Registration

class Fid_Calibration(object):

    def __init__(self, emfid_name, ctfid_name):
        self.emfid_name = emfid_name
        self.ctfid_name = ctfid_name
        self.em_frames = []
        self.ct_frame = None
        self.parse()
    # Find average and vectors relative to average of clouds list
    def cloud_computation(self, clouds):
        for c in clouds:
            c.compute_average()
            c.compute_relative_vectors()
    # Parse in data from files
    def parse(self):
        fid_parser = EMFid.EMFid_Parser(self.emfid_name)
        self.em_frames = fid_parser.return_frames()
   
        ct_parser = CTFid.CTFid_Parser(self.ctfid_name)
        self.ct_frame = ct_parser.return_frame()

    # Acquire P_tip with respect to EM tracker, using reference
    # g_cloud computed from EM Pivot Calibration
    # For each frame find F[k] and multiply by fixed t_g to find P_dimp
    def tip_points(self, t_g, g_cloud):
        P_points = []
        for frame in self.em_frames:
            G_cloud = frame.return_clouds()[0]
            self.cloud_computation([G_cloud])
            reg = Registration.Registration(G_cloud, g_cloud)
            (R, p) = reg.registration()
            P_points.append(R*t_g + p)
        return P_points

    # Given points in EM space and CT space, find F_reg using registration
    def find_F_reg(self, P_points):
        fixed_p_points = []
        for p in P_points:
            fixed_p_points.append(np.array([p[0,0], p[1,0], p[2,0]]))
        p_cloud = PointCloud.PointCloud(fixed_p_points)
        ct_cloud = self.ct_frame.return_clouds()[0]
        self.cloud_computation([p_cloud])
        self.cloud_computation([ct_cloud])
        reg = Registration.Registration(ct_cloud, p_cloud)
        (R,p) = reg.registration()
        return (R,p)
        
    # Return EM Frames
    def return_em_frames(self):
        return self.em_frames
    # Clear EM Frames, used for when the frames are dewarped
    def clear_em_frames(self):
        self.em_frames = []
    # Add a frame
    def add_frame(self, points):
        fr = Frame.Frame([points])
        self.em_frames.append(fr)
