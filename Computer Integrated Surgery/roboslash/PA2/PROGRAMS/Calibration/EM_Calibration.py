
#module EM_Calibration
import numpy as np
import Parser.EMPivot_Parser as EMPivot

import Containers.Frame as Frame
import Containers.PointCloud as PointCloud

import Transformation.Frame_Transformation as Frame_Transform

import Registration.Registration as Registration

class EM_Calibration(object):

    def __init__(self, em_name):
        self.em_name = em_name
        self.em_frames = []
        self.parse()

    def cloud_computation(self, clouds):
        for c in clouds:
            c.compute_average()
            c.compute_relative_vectors()

    def parse(self):
        emp_parser = EMPivot.EMPivot_Parser(self.em_name)
        self.em_frames = emp_parser.return_frames()

    def clear_frames(self):
        self.em_frames = []

    def return_frames(self):
        return self.em_frames

    def add_frame(self, points):
        fr = Frame.Frame([points])
        self.em_frames.append(fr)
 
    def calibrate(self):
        # Set up reference frame
        # g_j = G_j - G_mean
        g_cloud = self.reference_frame(self.em_frames[0].return_clouds()[0])
        R_list, p_list = [], []
        # Create a grand matrix and vector for least squares solution
        for frame in self.em_frames:
            # Get cloud from frame (there is only 1 cloud in this instance)
            G_cloud = frame.return_clouds()[0]
            self.cloud_computation([G_cloud])
            reg = Registration.Registration(G_cloud, g_cloud)
            (R, p) = reg.registration()
            R_list.append(R)
            p_list.append(p)
        # reorganization
        A_curr, p_curr = self.least_squares_matrix_organization(R_list, p_list)
        # perform least squares on A, p
        x = np.linalg.lstsq(A_curr, p_curr)
        # return P_dimp, t_g
        return x[0][3:,0], x[0][0:3:,0], g_cloud
        

    def reference_frame(self, X_cloud):
        self.cloud_computation([X_cloud])
        x_cloud = PointCloud.PointCloud(X_cloud.return_vectors_relative_average())
        self.cloud_computation([x_cloud])
        return x_cloud

    def least_squares_matrix_organization(self, R_list, p_list):
        A_matrices = []
        p_vectors = []

        # A = [R|-I] 
        for R in R_list:
            idt = np.matrix([ [-1, 0, 0], [0, -1, 0], [0, 0, -1] ])
            A = np.concatenate((R, idt), axis=1)
            A_matrices.append(A)

        for p in p_list:
            p = np.matrix([ [-1*p[0,0], -1*p[1,0], -1*p[2,0]] ])
            p_vectors.append(p)

        A_curr = A_matrices[0]
        for A in A_matrices[1:]:
            A_curr = np.concatenate((A_curr, A))
        p_curr = np.transpose(-1*p_vectors[0])
        for p in p_vectors[1:]:
            p_curr = np.concatenate((p_curr, np.transpose(-1*p)))
        return A_curr, p_curr
