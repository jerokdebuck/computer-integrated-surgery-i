-----------------------------------------
Darian Hadjiabadi // dhadjia1@gmail.com
Timothy Mullen // tmullen2@jhu.edu
600.445, Computer Integrated Surgery
Programming Assignment 2
-----------------------------------------

HOW TO RUN:
    
    This assignment was implemented using Python with extensive use of
    the numpy module. In the PROGRAMS/ directory you will find an
    executable 'main.py'. To run the program, perform the following

        $ ./main < input.txt
                         OR 
        $ python main.py < input.txt

    The main program has been tested on the ugrad cluster and should
    work on any machine as long as the python intrepeter is in
    /usr/bin. If not, the shebang line must be changed to include
    the location of the intrepeter. There will be two output files,
    one for PA1 and the other for PA2.

    The program will output 2 files: ....output1.txt and ....output2.txt
    in the same directory as main.py (PROGRAMS/). These will contain the
    output information

DIRECTORY HIERARCHY AND FILE DESCRIPTION:

DATA/ - Directory containing all input data

OUTPUT/ - Directory containing the output

PROGRAMS/ - Directory containing all source files
|
|
|---> cleanup.sh - Remove *.pyc files
|---> main.py - The driver python file to run the assignment
                Produces solutions in the form as described in pdf
|---> input.txt - File containing a list of input files (first seven) and 
                  output files (last 2).
                  User can change this as need be to run any set of input
                  files. Currently set to run pa1-a-debug-.. class files
|
|
---> Containers/ - Directory containing container classes for the assignment : Frame, PointCloud
      |
      |
      ---> Frame.py - Contains a simple Frame class to hold PointClouds
      ---> PointCloud.py - Contains PointCloud class which contains vectors
           in a given space as well as the average vector and its vectors
           relative to the average. 
|
---> Registration/ - Directory containing Registration class
     |
     |
     ---> Registration.py - A registration class that implements Horns method
|
---> Transforamtion/ - Directory containing Transformation class
     |
     |
     ---> Frame_Transformation.py - A transformation class that transforms
          one PointCloud to another given a rotation matrix and 
          translation vector
|
---> Parser/ - Directory containing various parser classes to parse input files
     |
     |---> Calbody_Parser.py - Class that parses calbody.txt files
     |---> Calreading_Parser.py - Class that parses calreadings.txt files
     |---> EMPivtor_Parser.py - Class that parses empivot.txt files
     |---> OPTPivot_Parser.py - Class that parses optpivot.txt files
     |---> EMFid_Parser.py - Class that parses em-fiducial.txt files
     |---> CTFid_Parser.py - Class that parses ct-fiducial.txt files
     |---> EMNav_Parser.py - Class that parses EM-nav.txt files
|
---> Calibration/ - Directory containing various Calibration classes and a class for dewarping
      |
      |---> Distortion_Calibration.py - Class to acquire C_expected values
      |---> EM_Calibration.py - Performs EM pivot calibration
      |---> Opt_Calibration.py - Performs optical pivot calibration
      |---> Fid_Calibration.py - Performs calibration on fiducials, finding F_reg between CT space and EM tracker space
      |---> Nav_Calibration.py - Performs calibration when nagivating in space, calculating the points of tool tip with respect to CT coordinates
      |---> Distortion.py - creates correction function and dewarps space
    
