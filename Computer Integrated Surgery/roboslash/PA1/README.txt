-----------------------------------------
Darian Hadjiabadi // dhadjia1@gmail.com
Timothy Mullen // tmullen2@jhu.edu
600.445, Computer Integrated Surgery
Programming Assignment 1
-----------------------------------------

HOW TO RUN:
    
    This assignment was implemented using Python with extensive use of
    the numpy module. In the PROGRAMS/ directory you will find an
    executable 'main.py'. To run the program, perform the following

        $ ./main < input.txt > {output_file_name}
                         OR 
        $ python main.py < input.txt > {output_file_name}

    The main program has been tested on the ugrad cluster and should
    work on any machine as long as the python intrepeter is in
    /usr/bin. If not, the shebang line must be changed to include
    the location of the intrepeter.

DIRECTORY HIERARCHY AND FILE DESCRIPTION:

DATA/ - Directory containing all input data

OUTPUT/ - Directory containing the output

PROGRAMS/ - Directory containing all source files
|
|
|---> cleanup.sh - Remove *.pyc files
|---> main.py - The driver python file to run the assignment
                Produces solutions in the form as described in pdf
|---> input.txt - File containing a list of input files (first four) and 
                  output file (last).
                  User can change this as need be to run any set of input
                  files. Currently set to run pa1-a-debug-.. class files
|
|
---> Containers/ - Directory containing container classes for the assignment : Frame, PointCloud
      |
      |
      ---> Frame.py - Contains a simple Frame class to hold PointClouds
      ---> PointCloud.py - Contains PointCloud class which contains vectors
           in a given space as well as the average vector and its vectors
           relative to the average. 
|
---> Registration/ - Directory containing Registration class
     |
     |
     ---> Registration.py - A registration class that implements Horns method
|
---> Transforamtion/ - Directory containing Transformation class
     |
     |
     ---> Frame_Transformation.py - A transformation class that transforms
          one PointCloud to another given a rotation matrix and 
          translation vector
|
---> Parser/ - Directory containing various parser classes to parse input files
     |
     |---> Calbody_Parser.py - Class that parses calbody.txt files
     |---> Calreading_Parser.py - Class that parses calreadings.txt files
     |---> EMPivtor_Parser.py - Class that parses empivot.txt files
     |---> OPTPivot_Parser.py - Class that parses optpivot.txt files

    
